﻿using System;
using System.Xml.Serialization;
using System.IO;
using ICSharpCode.SharpZipLib.Zip.Compression;

namespace dezentrale.model
{
    public class Blob
    {
        [XmlAttribute] public DateTime Added    { get; set; } = DateTime.Now;
        [XmlAttribute] public DateTime Modified { get; set; }
        [XmlElement]   public string FileName   { get; set; } = "";
        [XmlIgnore]    public string DataFileName
        {
            get
            {
                return $"{FileName}.xml";
            }
        }
        private BlobData data = null;
        [XmlIgnore]    public BlobData Data
        {
            get
            {
                if(data == null)
                {
                    data = new BlobData();
                }
                return data;
            }
            set
            {
                data = value;
            }
        }
        public Blob() : base() { }
        public Blob(string fileName) : base()
        {
            FileName = fileName;
            Added = Modified = DateTime.Now;
        }

        public bool SaveData()
        {
            string completePath = System.IO.Path.Combine(Program.config.DbDirectory, DataFileName);
            Program.config.DbChangedSinceExport = true;
            Program.config.LastDbLocalChange = DateTime.Now;
            XmlData.SaveToFile(Program.ConfigFile, Program.config);
            return XmlData.SaveToFile(completePath, Data);
        }
    }

    public class BlobData : XmlData
    {
        [XmlAttribute] public bool Compressed        { get; set; } = false;
        [XmlAttribute] public int DecompressedLength { get; set; } = 0;
                       public byte[] DataInternal    { get; set; }
        [XmlIgnore]    public byte[] Bytes
        {
            get
            {
                if(!Compressed)
                {
                    return DataInternal;
                } else
                {
                    //\todo This doesn't work right yet
                    Inflater i = new Inflater(false);
                    i.SetInput(DataInternal);
                    byte[] ret = new byte[DecompressedLength];

                    int len = i.Inflate(ret);
                    if (len != DecompressedLength) throw new Exception("Decompression of {DecompressedLength} Bytes using Inflater failed");
                    return ret;
                }
            }
            set
            {
                DecompressedLength = value.Length;
                if(true) //if (DecompressedLength == 0)
                {
                    Compressed = false;
                    DataInternal = value;
                }
                else
                {
                    //\todo This doesn't work right yet
                    Deflater d = new Deflater(Deflater.BEST_COMPRESSION, false);
                    d.SetInput(value);
                    byte[] output = new byte[DecompressedLength];
                    int len = d.Deflate(output, 0, DecompressedLength);
                    if (len < 0)
                    {
                        Console.WriteLine($"Deflate compression of {DecompressedLength} Bytes returned {len}, storing uncompressed");
                        Compressed = false;
                        DataInternal = value;
                    } else
                    {
                        Compressed = true;
                        byte[] di = new byte[len];
                        Buffer.BlockCopy(output, 0, di, 0, len);
                        DataInternal = di;
                    }
                }
            }
        }
    }
}
