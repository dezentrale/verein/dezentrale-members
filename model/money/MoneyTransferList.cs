﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace dezentrale.model.money
{
    public class MoneyTransferList : XmlData
    {
        [XmlElement("MoneyTransfer")] public List<MoneyTransfer> Entries = new List<MoneyTransfer>();
        [XmlIgnore] public static string FileName { get; } = "moneytransfers.xml";
        public static MoneyTransferList LoadFromFile()
        {
            try
            {
                string mtlFile = Path.Combine(Program.config.DbDirectory, FileName);
                return (MoneyTransferList)XmlData.LoadFromFile(mtlFile, typeof(MoneyTransferList));
            } catch(Exception ex)
            {
                Console.WriteLine($"Error while loading MoneyTransferList: {ex.Message}");
                return new MoneyTransferList();
            }
        }
        public static bool SaveToFile(MoneyTransferList list)
        {
            string mtlFile = Path.Combine(Program.config.DbDirectory, FileName);
            Program.config.DbChangedSinceExport = true;
            Program.config.LastDbLocalChange = DateTime.Now;
            XmlData.SaveToFile(Program.ConfigFile, Program.config);
            return XmlData.SaveToFile(mtlFile, list);
        }
        public bool SaveToFile()
        {
            return SaveToFile(this);
        }

        public void AddEntry(MoneyTransfer mt)
        {
            Entries.Add(mt);
            Entries.Sort();
        }
        public MoneyTransfer FindEqualEntry(MoneyTransfer other)
        {
            foreach (MoneyTransfer mt2 in Entries)
            {
                if (mt2.Equals(other))
                {
                    return mt2;
                }
            }
            return null;
        }
    }
}
