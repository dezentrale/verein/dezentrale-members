﻿using System;

namespace dezentrale.model
{
    public class MemberReport : FormMail
    {
        public MemberReport(bool memberList = false)
        {
            To = "{Nickname} <{EMail}>";
            Subject = $"Automatic member statistics {DateTime.Now.ToString("yyyy-MM-dd")}";

            int unGreetedMembers = 0;
            int activeMembers = 0;
            int regularMembers = 0;
            int foerderMembers = 0;
            int disabledMembers = 0;
            Int64 totalBalance = 0;
            Int64 totalSurplus = 0;
            Int64 totalDebts   = 0;
            Int64 minBalance = 0;
            Int64 maxBalance = 0;
            uint membersWithNegativeBalance = 0;

            string mList = "Members with non-0 account balance:\n\n"
                         + "ID  | balance  | status   | nickname\n"
                         + "----+----------+----------+----------------------\n";

            foreach (Member m in Program.members.Entries)
            {
                if (memberList && m.AccountBalance != 0)
                {
                    string formattedBalance = core.Utils.PadLeft(core.Utils.Int64FPToString(m.AccountBalance), 8);
                    //while (formattedBalance.Length < 8) formattedBalance = " " + formattedBalance;

                    string formattedStatus = core.Utils.PadLeft($"{m.Status}", 8);
                    //while (formattedStatus.Length < 8) formattedStatus = formattedStatus + " ";
                    mList += $"{m.Number:D3} | {formattedBalance} | {formattedStatus} | {m.Nickname}\n";
                }

                if (m.Status == Member.eStatus.Active)
                {
                    totalBalance += m.AccountBalance;
                    if (m.AccountBalance > 0)
                        totalSurplus += m.AccountBalance;
                    else
                        totalDebts += m.AccountBalance;
                    if (minBalance > m.AccountBalance) minBalance = m.AccountBalance;
                    if (maxBalance < m.AccountBalance) maxBalance = m.AccountBalance;
                    if (m.AccountBalance < 0) membersWithNegativeBalance++;
                }

                switch (m.Status)
                {
                    case Member.eStatus.Uninitialized:
                    case Member.eStatus.Greeted:
                    {
                        unGreetedMembers++;
                    } break;
                    case Member.eStatus.Active:
                    {
                        activeMembers++;
                        if (m.Type == Member.eType.Regulaer) regularMembers++;
                        if (m.Type == Member.eType.Foerdermitglied) foerderMembers++;
                    } break;
                    case Member.eStatus.Disabled:
                    {
                        disabledMembers++;
                    } break;
                    default: break;
                }
            }
            Body =
                  $"Out of {Program.members.Entries.Count}, there are {activeMembers} active members.\n"
                + $"Out of these, {regularMembers} are regular and {foerderMembers} are \"Foerdermitglied\"\n"
                + $"{unGreetedMembers} members are \"uninitialized or greeted\", waiting for spawn/activation.\n"
                + $"{disabledMembers} accounts are disabled at this point.\n"
                + $"The next member number will be {Program.members.GetFreeNumber():D3}\n\n"
                + $"Account balances (active members):\n"
                + $"----------------------------------\n"
                + $"Total balance of membership fees: {( (float) totalBalance / 100)} EUR (sum of all balances) this is the sum of:\n"
                + $"Total surplus of membership fees: {( (float) totalSurplus / 100)} EUR (paid for the following months)\n"
                + $"Total debts   of membership fees: {( (float) totalDebts   / 100)} EUR (missing payments)\n\n"
                + $"Members with negative balance: {membersWithNegativeBalance} people\n"
                + $"Peak negative balance: {( (float) minBalance / 100)} EUR\n"
                + $"Peak positive balance: {( (float) maxBalance / 100)} EUR\n\n"
                + $"{(memberList ? mList : "")}\n";
        }
    }
}
