﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace dezentrale.model
{
    public class LogEvent : LogSubEvent
    {
        public enum eEventType
        {
            Generic = 0,
            AddUser,
            EditUser,
            DelUser,

            DataChange,

            Greetings,
            Activation,
            Deactivation,

            MembershipFee,
            MembershipPayment,
            MembershipDonation,

            EMail,

            // TODO: [Obsolete("Use ELogLevel.Error instead")]
            Error,
        }

        public enum ELogLevel
        {
            Trace = -1,
            Debug = 0,
            Info = 1,
            Warning = 2,
            Error = 3,
        }

        [XmlAttribute] public DateTime Timestamp { get; set; } = DateTime.Now;
        [XmlAttribute] public string LocalUser { get; set; } = Program.config.LocalUser;
        [XmlElement("SubEvent")] public List<LogSubEvent> SubEvents { get; set; } = new List<LogSubEvent>();
    }
}
