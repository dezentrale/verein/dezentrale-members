﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace dezentrale.model
{
    public class Configuration : XmlData
    {
        [XmlElement] public ConfigSmtp Smtp                { get; set; } = new ConfigSmtp();
        [XmlElement] public ConfigVSMail VS                { get; set; } = new ConfigVSMail();
        [XmlElement] public MemberImportExport ImportExport{ get; set; } = new MemberImportExport();

        [XmlElement] public string DbDirectory             { get; set; } = DefaultDbDirectory;
        //[XmlElement] public string DbBackupDirectory       { get; set; } = DefaultDbBackupDirectory;
        //[XmlElement] public string ImportExportDirectory   { get; set; } = DefaultImportExportDirectory;
        [XmlElement] public uint   RegularPaymentAmount    { get; set; } = 3200; //cents
        [XmlElement] public string RegularPaymentCurrency  { get; set; } = "EUR";
        [XmlElement] public string LocalUser               { get; set; } = "John Doe";
        [XmlElement] public string KeylockCombination      { get; set; } = "0000";


        //UI: lstMembers: Columns
        [XmlElement("MemberListColumn")] public List<ConfigLVColumn> MemberListColumns { get; set; } = new List<ConfigLVColumn>();
        [XmlElement("MTListColumn")]     public List<ConfigLVColumn> MTListColumns     { get; set; } = new List<ConfigLVColumn>();
        [XmlElement("MvListColumn")]     public List<ConfigLVColumn> MvListColumns     { get; set; } = new List<ConfigLVColumn>();
        [XmlElement("MvInvitationsListColumn")]
                                   public List<ConfigLVColumn> MvInvitationsListColumns{ get; set; } = new List<ConfigLVColumn>();

        [XmlElement] public List<KeyValue> MoneyTransferRegEx { get; set; } = new List<KeyValue>();    //This doesn't belong here! Move to new file within db-data!
        [XmlElement] public DateTime LastCronjobRun        { get; set; } = DateTime.Now;    //This doesn't belong here! Move to new file within db-data!

        [XmlElement] public DateTime LastDbLocalChange     { get; set; }
        [XmlElement] public DateTime LastDbExport          { get; set; }
        [XmlElement] public DateTime LastDbImport          { get; set; }
        [XmlElement] public bool DbChangedSinceExport      { get; set; } = false;

        [XmlIgnore] public static string DefaultDbDirectory           { get; private set; } = "db-data";
        //[XmlIgnore] public static string DefaultDbBackupDirectory     { get; private set; } = "db-backup";
        //[XmlIgnore] public static string DefaultImportExportDirectory { get; private set; } = "import-export";
    }
}
