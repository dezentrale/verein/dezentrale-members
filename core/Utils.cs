﻿using System;
namespace dezentrale.core
{
    public class Utils
    {
        public static string PadLeft(string input, uint totalLength)
        {
            if (input.Length < totalLength)
                return new string(' ', (int)(totalLength - input.Length)) + input;
            else
                return input;
        }
        //TBD: This is an utility function and should reside in an extra class
        public static string Int64FPToString(Int64 value)
        {
            //float fValue = ((float) value) / 100;
            //return $"{fValue:0.00}";
            Int64 intPart = value / 100;
            Int64 decPart = (intPart < 0 ? -1 : 1) * (value % 100);
            return $"{intPart},{decPart:D2}";
        }
        //TBD: This is an utility function and should reside in an extra class
        public static Int64 StringToInt64FP(string value)
        {
            if (value == null) return 0;
            if (!value.Contains(".") && !value.Contains(","))
            {
                return Convert.ToInt64(value) * 100;
            }

            int i = value.IndexOf('.');
            if (i < 0) i = value.IndexOf(',');
            string intPart = value.Substring(0, i);
            string decPart = value.Substring(i + 1);

            Int64 ip64 = Convert.ToInt64(intPart);
            return (ip64 * 100) + (ip64 > 0 ? Convert.ToInt64(decPart) : -Convert.ToInt64(decPart));
        }

    }
}
