﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using dezentrale.model;

namespace dezentrale.view
{
    public class frmConfigureLVColumns : Form
    {
        public bool ColumnsChanged { get; private set; } = false;
        private ListView columns;
        private ContextMenu cm;
        private MenuItem cmMoveUpEntry;
        private MenuItem cmMoveDownEntry;
        private bool orderingChanged = false;

        public frmConfigureLVColumns(List<ConfigLVDataHandler> allColumns, string configuredLV = null)
        {
            this.Text = configuredLV ?? "Columns";
            this.Size = new System.Drawing.Size(400, 300);
            this.StartPosition = FormStartPosition.CenterParent;
            this.FormClosing += FrmConfigureLVColumns_FormClosing;

            this.Controls.Add(columns = new ListView()
            {
                View = View.Details, //This gives us a traditional "list" view.
                Dock = DockStyle.Fill,
                CheckBoxes = true,
                FullRowSelect = true,
                MultiSelect = true,
                Columns =
                {
                    new ColumnHeader() { Text = "vis",     Width = 26,  },
                    new ColumnHeader() { Text = "Display", Width = 336, }
                }
            });
            columns.SelectedIndexChanged += Columns_SelectedIndexChanged;
            columns.ItemCheck += Columns_ItemCheck;


            cmMoveUpEntry = new MenuItem(@"/\ Move up", cmMoveUp);
            cmMoveDownEntry = new MenuItem(@"\/ Move Down", cmMoveDown);

            cm = new ContextMenu()
            {
                MenuItems =
                {
                    cmMoveUpEntry,
                    cmMoveDownEntry,
                    new MenuItem("-"),
                    new MenuItem("[x] Visible", cmSelect),
                    new MenuItem("[ ] Invisible", cmDeSelect),
                }
            };

            foreach (ConfigLVDataHandler col in allColumns)
            {
                ListViewItem lvi;
                columns.Items.Add(lvi = new ListViewItem()
                {
                    Tag = col,
                    Checked = col.Visible,
                });
                lvi.SubItems.Add(new ListViewItem.ListViewSubItem(lvi, col.Display));
            }

        }

        private void FrmConfigureLVColumns_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (orderingChanged) { ColumnsChanged = true; return; }
            foreach(ListViewItem lvi in columns.Items)
            {
                ConfigLVColumn lvc = (ConfigLVColumn)lvi.Tag;
                if(lvc.Visible != lvi.Checked)
                {
                    ColumnsChanged = true; return;
                }
            }
        }
        public List<ConfigLVDataHandler> GetLVColumnList()
        {
            List<ConfigLVDataHandler> ret = new List<ConfigLVDataHandler>();
            foreach (ListViewItem lvi in columns.Items)
            {
                ConfigLVDataHandler lvc = (ConfigLVDataHandler)lvi.Tag;
                lvc = new ConfigLVDataHandler(lvc, lvc);   //we need to make a copy here!
                lvc.Visible = lvi.Checked;
                ret.Add(lvc);
            }
            return ret;
        }

        private void cmMoveUp(object sender, EventArgs e)
        {
            int startIndex = columns.SelectedIndices[0];
            if (startIndex < 1) return; else startIndex--;
            orderingChanged = true;
            ListView.SelectedListViewItemCollection sel = columns.SelectedItems;
            foreach (ListViewItem lvi in sel)
            {
                columns.Items.Remove(lvi);
                columns.Items.Insert(startIndex++, lvi);
            }
        }
        private void cmMoveDown(object sender, EventArgs e)
        {
            int startIndex = columns.SelectedIndices[columns.SelectedIndices.Count - 1];
            if (startIndex > (columns.Items.Count - 2)) return; else startIndex++;
            orderingChanged = true;
            ListView.SelectedListViewItemCollection sel = columns.SelectedItems;
            foreach (ListViewItem lvi in sel)
            {
                columns.Items.Remove(lvi);
                columns.Items.Insert(startIndex, lvi);
            }
        }
        private void cmSelect(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection sel = columns.SelectedItems;
            foreach(ListViewItem lvi in sel)
                lvi.Checked = true;
        }
        private void cmDeSelect(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection sel = columns.SelectedItems;
            foreach (ListViewItem lvi in sel)
                lvi.Checked = false;
        }

        private void Columns_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //This is a workaround for checkboxes keep changing when MultiSelect with Shift
            if ((ModifierKeys & (Keys.Shift | Keys.Control)) != 0)
                e.NewValue = e.CurrentValue;
        }

        private void Columns_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (columns.SelectedIndices.Count < 1)
                columns.ContextMenu = null;
            else
            {
                columns.ContextMenu = cm;

                cmMoveUpEntry.Enabled = columns.SelectedIndices[0] > 0;
                cmMoveDownEntry.Enabled = columns.SelectedIndices[columns.SelectedIndices.Count - 1] < (columns.Items.Count - 1);
            }
        }
    }
}
