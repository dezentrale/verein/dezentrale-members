﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using dezentrale.model;

namespace dezentrale.view
{
    public class frmConfiguration : FormWithActionButtons
    {
        //Generic settings
        private TextBox tbDbDirectory;
        private TextBox tbRegularPayment;
        private ComboBox cbRegularCurrency;
        private TextBox tbLocalUser;
        private TextBox tbKeylockCombination;
        public bool KeylockCombiChanged { get; private set; } = false;

        //SMTP settings
        private CheckBox cbSmtpEnabled;
        private TextBox tbSmtpHost;
        private TextBox tbSmtpPort;
        private CheckBox cbSmtpSSL;
        private TextBox tbSmtpFrom;
        private TextBox tbSmtpUserName;
        private TextBox tbSmtpPassword;
        private TextBox tbSmtpCcTo;

        //Vorstand settings
        private TextBox tbVSName;
        private TextBox tbVSEmail;

        //Import / Export settings
        private TextBox tbIeZipFile;
        private TextBox tbIeZipPassword;
        private CheckBox cbIeGpgEnabled;
        private TextBox tbIeGpgFile;
        private TextBox tbIeGpgPassword;

        private CheckBox cbIeHgEnabled;
        private TextBox tbIeHgUserName;
        private TextBox tbIeHgPassword;
        private TextBox tbIeHgURL;

        public TabPage BuildGenericGui()
        {
            TabPage gui = new TabPage("Generic");
            gui.Controls.Add(new Label()
                {
                    Text = "DbDirectory:",
                    Location = new Point(lm, 0 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbDbDirectory = new TextBox()
                {
                    Location = new Point(lm + 113, 0 * line + tm),
                    Width = 400,
                });
            Button btnBrowseDataDir;
            gui.Controls.Add(btnBrowseDataDir = new Button()
                {
                    Text = "...",
                    Location = new Point(lm + 523, 0 * line + tm),
                    Width = 30,
                });
            btnBrowseDataDir.Click += btnBrowseDataDir_Click;
            gui.Controls.Add(new Label()
                {
                    Text = "RegularPayment:",
                    Location = new Point(lm, 1 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbRegularPayment = new TextBox()
                {
                    Text = "0,00",
                    Location = new Point(lm + 113, 1 * line + tm),
                    Width = 100,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    TextAlign = HorizontalAlignment.Right,
                });
            gui.Controls.Add(cbRegularCurrency = new ComboBox()
                {
                    Location = new Point(lm + 223, 1 * line + tm),
                    Width = 70,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    DropDownStyle = ComboBoxStyle.DropDownList,
                    ForeColor = Color.Black,
                });
            cbRegularCurrency.Items.Add("EUR");
            cbRegularCurrency.SelectedIndex = 0;

            gui.Controls.Add(new Label()
                {
                    Text = "LocalUser:",
                    Location = new Point(lm, 2 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbLocalUser = new TextBox()
                {
                    Location = new Point(lm + 113, 2 * line + tm),
                    Width = 100,
                });
            gui.Controls.Add(new Label()
                {
                    Text = "Doorlock code:",
                    Location = new Point(lm, 3 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbKeylockCombination = new TextBox()
            {
                Location = new Point(lm + 113, 3 * line + tm),
                Width = 100,
            });

            tbDbDirectory.Text = Program.config.DbDirectory;
            tbRegularPayment.Text = core.Utils.Int64FPToString(Program.config.RegularPaymentAmount);
            if (Program.config.RegularPaymentCurrency != "EUR")
            {
                cbRegularCurrency.Items.Add(Program.config.RegularPaymentCurrency);
                cbRegularCurrency.SelectedIndex = 1;
            }
            tbLocalUser.Text = Program.config.LocalUser;
            tbKeylockCombination.Text = Program.config.KeylockCombination;
            return gui;
        }

        public TabPage BuildSmtpGui()
        {
            TabPage gui = new TabPage("Mail Settings");
            gui.Controls.Add(cbSmtpEnabled = new CheckBox()
                {
                    Text = "Enable SMTP transmission",
                    Location = new Point(lm + 113, 0 * line + tm),
                    Width = 400,
                });
            
            gui.Controls.Add(new Label()
                {
                    Text = "SMTP Server:",
                    Location = new Point(lm, 1 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbSmtpHost = new TextBox()
                {
                    Text = "localhost",
                    Location = new Point(lm + 113, 1 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            gui.Controls.Add(tbSmtpPort = new TextBox()
                {
                    Text = "587",
                    Location = new Point(lm + 318, 1 * line + tm),
                    Width = 40,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });

            gui.Controls.Add(cbSmtpSSL = new CheckBox()
                {
                    Text = "Enable encrypted connection (SSL)",
                    Location = new Point(lm + 113, 2 * line + tm),
                    Width = 400,
                });
            gui.Controls.Add(new Label()
                {
                    Text = "Mail from:",
                    Location = new Point(lm, 3 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbSmtpFrom = new TextBox()
                {
                    Location = new Point(lm + 113, 3 * line + tm),
                    Width = 400,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            
            gui.Controls.Add(new Label()
                {
                    Text = "SMTP login:",
                    Location = new Point(lm, 4 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbSmtpUserName = new TextBox()
                {
                    Location = new Point(lm + 113, 4 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,});
            
            gui.Controls.Add(new Label()
                {
                    Text = "SMTP password:",
                    Location = new Point(lm, 5 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbSmtpPassword = new TextBox()
                {
                    PasswordChar = '*',
                    Location = new Point(lm + 113, 5 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
                        
            gui.Controls.Add(new Label()
                {
                    Text = "Cc sent mails:",
                    Location = new Point(lm, 6 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbSmtpCcTo = new TextBox()
                {
                    Location = new Point(lm + 113, 6 * line + tm),
                    Width = 400,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });

            //VS settings
            gui.Controls.Add(new Label()
                {
                    Text = "Contact info for the Vorstand",
                    Location = new Point(lm, 8 * line + tm + labelOffs),
                    Size = new Size(210, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
                        
            gui.Controls.Add(new Label()
                {
                    Text = "Name:",
                    Location = new Point(lm, 9 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbVSName = new TextBox()
                {
                    Location = new Point(lm + 113, 9 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            gui.Controls.Add(new Label()
                {
                    Text = "E-Mail:",
                    Location = new Point(lm, 10 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbVSEmail = new TextBox()
                {
                    Location = new Point(lm + 113, 10 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });

            cbSmtpEnabled.Checked = Program.config.Smtp.Enabled;
            tbSmtpHost.Text = Program.config.Smtp.Host;
            tbSmtpPort.Text = $"{Program.config.Smtp.Port}";
            cbSmtpSSL.Checked = Program.config.Smtp.SSL;
            tbSmtpFrom.Text = Program.config.Smtp.From;
            tbSmtpUserName.Text = Program.config.Smtp.UserName;
            tbSmtpPassword.Text = Program.config.Smtp.Password;
            tbSmtpCcTo.Text = Program.config.Smtp.CcTo;

            tbVSName.Text = Program.config.VS.VSName;
            tbVSEmail.Text = Program.config.VS.VSEmail;
            return gui;
        }
        public TabPage BuildMoneyTransfersGui()
        {
            TabPage gui = new TabPage("MoneyTransfers");
            //use MoneyTransferList for that, not Program.config
            //[XmlElement] public List<KeyValue> MoneyTransferRegEx { get; set; } = new List<KeyValue>();
            return gui;
        }
        public TabPage BuildImportExportGui()
        {
            TabPage gui = new TabPage("Import / Export");
            
            gui.Controls.Add(new Label()
                {
                    Text = "Zip filename:",
                    Location = new Point(lm, 0 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbIeZipFile = new TextBox()
                {
                    Location = new Point(lm + 113, 0 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            
            gui.Controls.Add(new Label()
                {
                    Text = "Zip password:",
                    Location = new Point(lm, 1 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbIeZipPassword = new TextBox()
                {
                    PasswordChar = '*',
                    Location = new Point(lm + 113, 1 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            
            gui.Controls.Add(cbIeGpgEnabled = new CheckBox()
                {
                    Text = "Enable file encryption with GPG (AES + password)",
                    Location = new Point(lm + 113, 2 * line + tm),
                    Width = 400,
                });
            
            gui.Controls.Add(new Label()
                {
                    Text = "Gpg filename:",
                    Location = new Point(lm, 3 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbIeGpgFile = new TextBox()
                {
                    Location = new Point(lm + 113, 3 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            
            gui.Controls.Add(new Label()
                {
                    Text = "Gpg password:",
                    Location = new Point(lm, 4 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbIeGpgPassword = new TextBox()
                {
                    PasswordChar = '*',
                    Location = new Point(lm + 113, 4 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            gui.Controls.Add(cbIeHgEnabled = new CheckBox()
                {
                    Text = "Enable data synchronisation over mercurial",
                    Location = new Point(lm + 113, 5 * line + tm),
                    Width = 400,
                });
            
            gui.Controls.Add(new Label()
                {
                    Text = "HG UserName:",
                    Location = new Point(lm, 6 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbIeHgUserName = new TextBox()
                {
                    Location = new Point(lm + 113, 6 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            
            gui.Controls.Add(new Label()
                {
                    Text = "HG password:",
                    Location = new Point(lm, 7 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbIeHgPassword = new TextBox()
                {
                    PasswordChar = '*',
                    Location = new Point(lm + 113, 7 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            
            gui.Controls.Add(new Label()
                {
                    Text = "HG URL:",
                    Location = new Point(lm, 8 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            gui.Controls.Add(tbIeHgURL = new TextBox()
                {
                    Location = new Point(lm + 113, 8 * line + tm),
                    Width = 400,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });

            tbIeZipFile.Text = Program.config.ImportExport.ZipFile;
            tbIeZipPassword.Text = Program.config.ImportExport.ZipPassword;
            cbIeGpgEnabled.Checked = Program.config.ImportExport.GpgEnabled;
            tbIeGpgFile.Text = Program.config.ImportExport.GpgFile;
            tbIeGpgPassword.Text = Program.config.ImportExport.GpgPassword;
            cbIeHgEnabled.Checked = Program.config.ImportExport.HgEnabled;
            tbIeHgUserName.Text = Program.config.ImportExport.HgUserName;
            tbIeHgPassword.Text = Program.config.ImportExport.HgPassword;
            tbIeHgURL.Text = Program.config.ImportExport.HgURL;
            /*
        [XmlElement] public bool   GitEnabled   { get; set; } = false;
        [XmlElement] public string GitUserName  { get; set; } = "";
        [XmlElement] public string GitPassword  { get; set; } = "";*/
            return gui;
        }
        public frmConfiguration()
        {
            DialogResult = DialogResult.Cancel;
            this.StartPosition = FormStartPosition.CenterParent;
            this.Size = new System.Drawing.Size(800, 600);
            this.Text = "dezentrale-members :: Configuration";
            this.Controls.Add(new TabControl()
            {
                Size = new System.Drawing.Size(this.Width - 16, this.Height - 95),
                TabPages =
                    {
                        BuildGenericGui(),
                        BuildSmtpGui(),
                        BuildMoneyTransfersGui(),
                        BuildImportExportGui(),
                    }
                });
            AddButton("OK", btnOK_Click);
            AddButton("Cancel", btnCancel_Click);
        }
        private void btnOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            this.Close();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnBrowseDataDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult dr = fbd.ShowDialog();
            if(dr == DialogResult.OK)
            {
                tbDbDirectory.Text = fbd.SelectedPath;
            }
        }
        public void FillAndSaveConfig()
        {
            Program.config.DbDirectory = tbDbDirectory.Text;
            Program.config.RegularPaymentAmount = (uint) core.Utils.StringToInt64FP(tbRegularPayment.Text);
            Program.config.RegularPaymentCurrency = cbRegularCurrency.Text;
            Program.config.LocalUser = tbLocalUser.Text;
            if (Program.config.KeylockCombination != tbKeylockCombination.Text)
                KeylockCombiChanged = true;
            Program.config.KeylockCombination = tbKeylockCombination.Text;

            Program.config.Smtp.Enabled = cbSmtpEnabled.Checked;
            Program.config.Smtp.Host = tbSmtpHost.Text;
            Program.config.Smtp.Port = Convert.ToUInt16(tbSmtpPort.Text);
            Program.config.Smtp.SSL = cbSmtpSSL.Checked;
            Program.config.Smtp.From = tbSmtpFrom.Text;
            Program.config.Smtp.UserName = tbSmtpUserName.Text;
            Program.config.Smtp.Password = tbSmtpPassword.Text;
            Program.config.Smtp.CcTo = tbSmtpCcTo.Text;

            Program.config.VS.VSName = tbVSName.Text;
            Program.config.VS.VSEmail = tbVSEmail.Text;

            //use MoneyTransferList for that, not Program.config
            //[XmlElement] public List<KeyValue> MoneyTransferRegEx { get; set; } = new List<KeyValue>();

            Program.config.ImportExport.ZipFile = tbIeZipFile.Text;
            Program.config.ImportExport.ZipPassword = tbIeZipPassword.Text;
            Program.config.ImportExport.GpgEnabled = cbIeGpgEnabled.Checked;
            Program.config.ImportExport.GpgFile = tbIeGpgFile.Text;
            Program.config.ImportExport.GpgPassword = tbIeGpgPassword.Text;
            Program.config.ImportExport.HgEnabled = cbIeHgEnabled.Checked;
            Program.config.ImportExport.HgUserName = tbIeHgUserName.Text;
            Program.config.ImportExport.HgPassword = tbIeHgPassword.Text;
            Program.config.ImportExport.HgURL = tbIeHgURL.Text;
            /*
        [XmlElement] public bool   GitEnabled   { get; set; } = false;
        [XmlElement] public string GitUserName  { get; set; } = "";
        [XmlElement] public string GitPassword  { get; set; } = "";*/


            try
            {
                XmlData.SaveToFile(Program.ConfigFile, Program.config);
                Program.MoneyTransfers.SaveToFile();
                Console.WriteLine("Stored new configuration.");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error while storing configuration: {ex.Message}");
            }
        }
    }
}
