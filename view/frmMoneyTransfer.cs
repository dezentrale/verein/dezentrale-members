﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using dezentrale.model;
using dezentrale.model.money;

namespace dezentrale.view
{
    public class frmMoneyTransfer : Form
    {
        private MoneyTransfer mt = null;
        private bool writeEnabled = true;
        private Type mtType = typeof(MoneyTransfer);
        private Member assignedTo = null;

        //Basic fields
        //Id           - auto-assigned
        private   ComboBoxKV     cbAssigned;
        private   ComboBoxKV     cbType; //!< Selection of @ref MoneyTransfer.eTransferType
        private   DateTimePicker valutaDate;
        private   TextBox        tbAmount;
        private   ComboBox       cbCurrency;
        private   TextBox        tbTransferReason;

        //CashTransfer fields
        private TextBox tbCashRecipeNumber;

        //BankTransfer fields
        private TextBox tbBankAccountInCharge;
        private DateTimePicker bankBookingDate;
        private TextBox tbBankBookingText;
        private TextBox tbBankCreditorID;
        private TextBox tbBankMandateReference;
        private TextBox tbBankClientReference;
        private TextBox tbBankSammlerreferenz;
        private TextBox tbBankLastschriftUrsprungsbetrag;
        private TextBox tbBankAuslagenersatzRuecklastschrift;
        private TextBox tbBankRecipientOrDebitor;
        private TextBox tbBankIBAN;
        private TextBox tbBankBIC;
        private TextBox tbBankInfo;

        private const int margin = 5;
        private const int lm = margin;   //Left margin
        private const int tm = margin;   //Top margin
        private const int rm = margin;   //Right margin
        private const int bm = margin;   //Bottom margin
        private const int line = 20 + margin;    //lineheight
        private const int labelHeight = 14;
        private const int labelOffs = 3;

        private void AddOkCancel(Control parent, EventHandler okEvent, EventHandler cancelEvent, bool writeEnabled)
        {
            //Console.WriteLine($"frmMoneyTransfer.AddOkCancel(writeEnabled={writeEnabled})");
            Button ok = new Button()
            {
                Text = "OK",
                Location = new System.Drawing.Point(parent.Width - 170 - rm, parent.Height - 87),
                Anchor = AnchorStyles.Right | AnchorStyles.Bottom,
                Enabled = writeEnabled,
            };
            if (writeEnabled) ok.Click += okEvent;

            Button cancel = new Button()
            {
                Text = "Cancel",
                Location = new System.Drawing.Point(parent.Width - 85 - rm, parent.Height - 87),
                Anchor = AnchorStyles.Right | AnchorStyles.Bottom,
            };
            cancel.Click += cancelEvent;

            ok.Height += 2;
            cancel.Height += 2;
            parent.Controls.Add(ok);
            parent.Controls.Add(cancel);
        }
        private TabPage BuildBankTransferGui()
        {
            //Console.WriteLine($"frmMoneyTransfer.BuildBankTransferGui(writeEnabled={writeEnabled})");
            TabPage bank = new TabPage()
                {
                    Text = "BankTransfer",
                };

            bank.Controls.Add(new Label()
                {
                    Text = "Account In Charge:",
                    Location = new Point(lm, 0 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankAccountInCharge = new TextBox()
                {
                    Location = new Point(lm + 113, 0 * line + tm),
                    Width = 100,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "BookingDate:",
                    Location = new Point(lm, 1 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(bankBookingDate = new DateTimePicker()
                {
                    Location = new Point(lm + 113, 1 * line + tm),
                    Width = 100,
                    Format = DateTimePickerFormat.Short,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "Booking Text:",
                    Location = new Point(lm, 2 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankBookingText = new TextBox()
                {
                    Location = new Point(lm + 113, 2 * line + tm),
                    Width = 100,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "Creditor ID:",
                    Location = new Point(lm, 3 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankCreditorID = new TextBox()
                {
                    Location = new Point(lm + 113, 3 * line + tm),
                    Width = 100,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "MandateReference:",
                    Location = new Point(lm, 4 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankMandateReference = new TextBox()
                {
                    Location = new Point(lm + 113, 4 * line + tm),
                    Width = 100,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "ClientReference:",
                    Location = new Point(lm, 5 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankClientReference = new TextBox()
                {
                    Location = new Point(lm + 113, 5 * line + tm),
                    Width = 100,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "Sammlerreferenz:",
                    Location = new Point(lm, 6 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankSammlerreferenz = new TextBox()
                {
                    Location = new Point(lm + 113, 6 * line + tm),
                    Width = 100,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "LastschriftUrsprungsbetrag:",
                    Location = new Point(lm + 308, 0 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankLastschriftUrsprungsbetrag = new TextBox()
                {
                    Location = new Point(lm + 421, 0 * line + tm),
                    Width = 100,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "Auslagenersatz Ruecklastschrift:",
                    Location = new Point(lm + 308, 1 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankAuslagenersatzRuecklastschrift = new TextBox()
                {
                    Location = new Point(lm + 421, 1 * line + tm),
                    Width = 100,
                });
            
            bank.Controls.Add(new Label()
                {
                    Text = "RecipientOrDebitor:",
                    Location = new Point(lm + 308, 2 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankRecipientOrDebitor = new TextBox()
                {
                    Location = new Point(lm + 421, 2 * line + tm),
                    Width = 100,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "IBAN:",
                    Location = new Point(lm + 308, 3 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankIBAN = new TextBox()
                {
                    Location = new Point(lm + 421, 3 * line + tm),
                    Width = 100,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "BIC:",
                    Location = new Point(lm + 308, 4 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankBIC = new TextBox()
                {
                    Location = new Point(lm + 421, 4 * line + tm),
                    Width = 100,
                });

            bank.Controls.Add(new Label()
                {
                    Text = "Info:",
                    Location = new Point(lm + 308, 5 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            bank.Controls.Add(tbBankInfo = new TextBox()
                {
                    Location = new Point(lm + 421, 5 * line + tm),
                    Width = 100,
                });
            AddOkCancel(bank, btnBankOK_Click, btnCancel_Click, writeEnabled);

            if (mt != null && mt.GetType() == typeof(BankTransfer))
            {
                BankTransfer bt = (BankTransfer)mt;
                tbBankAccountInCharge.Text = bt.AccountInCharge;
                try { bankBookingDate.Value = bt.BookingDate; } catch (Exception ex) { MessageBox.Show(ex.Message); }
                tbBankBookingText.Text = bt.BookingText;
                tbBankCreditorID.Text = bt.CreditorID;
                tbBankMandateReference.Text = bt.MandateReference;
                tbBankClientReference.Text = bt.ClientReference;
                tbBankSammlerreferenz.Text = bt.Sammlerreferenz;
                tbBankLastschriftUrsprungsbetrag.Text = bt.LastschriftUrsprungsbetrag;
                tbBankAuslagenersatzRuecklastschrift.Text = bt.AuslagenersatzRuecklastschrift;
                tbBankRecipientOrDebitor.Text = bt.RecipientOrDebitor;
                tbBankIBAN.Text = bt.IBAN;
                tbBankBIC.Text = bt.BIC;
                tbBankInfo.Text = bt.Info;
            }

            return bank;
        }
        private TabPage BuildCashTransferGui()
        {
            TabPage cash = new TabPage()
                {
                    Text = "CashTransfer",
                };
            cash.Controls.Add(new Label()
                {
                    Text = "Recipe Number:",
                    Location = new Point(lm, 0 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            cash.Controls.Add(tbCashRecipeNumber = new TextBox()
                {
                    Location = new Point(lm + 113, 0 * line + tm),
                    Width = 100,
                });
            AddOkCancel(cash, btnCashOK_Click, btnCancel_Click, writeEnabled);

            if (mt != null && mt.GetType() == typeof(CashTransfer))
            {
                CashTransfer ct = (CashTransfer)mt;
                tbCashRecipeNumber.Text = ct.RecipeNumber;
            }

            return cash;
        }

        private void BuildMoneyTransferGui()
        {
            this.Size = new Size(600, 400);
            this.StartPosition = FormStartPosition.CenterParent;
            this.Text = (assignedTo != null ? $"{assignedTo.Number:D3} ({assignedTo.Nickname}) - " : "Not assigned - ")
            + (mt == null ? "Add" : (writeEnabled ? "Edit" : "View"))
            + " Money Transfer";

            this.Controls.Add(new Label()
                {
                    Text = "Assigned to:",
                    Location = new Point(lm, 0 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            this.Controls.Add(cbAssigned = new ComboBoxKV()
                {
                    Location = new Point(lm + 118, 0 * line + tm),
                    Width = 180,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    DropDownStyle = ComboBoxStyle.DropDownList,
                    Enabled = mt == null ? true : !mt.AssignFixed,
                });
            cbAssigned.AddKV("- Unassigned -", 0);
            cbAssigned.SelectedIndex = 0;
            foreach(Member m in Program.members.Entries)
            {
                cbAssigned.AddKV($"{m.Number:D3} ({m.Nickname})", m.Number);
                if (m == assignedTo)
                {
                    cbAssigned.SelectedIndex = cbAssigned.Items.Count - 1;
                    cbAssigned.Enabled = false;
                }
            }

            this.Controls.Add(new Label()
                {
                    Text = "Type:",
                    Location = new Point(lm, 1 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            this.Controls.Add(cbType = new ComboBoxKV()
                {
                    Location = new Point(lm + 118, 1 * line + tm),
                    Width = 180,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    DropDownStyle = ComboBoxStyle.DropDownList,
                    Enabled = mt == null ? true : !mt.AssignFixed,
                });

            this.Controls.Add(new Label()
                {
                    Text = "ValutaDate:",
                    Location = new Point(lm, 2 * line + tm + labelOffs),
                    Size = new Size(110, labelOffs),
                    TextAlign = ContentAlignment.BottomRight,
                });
            this.Controls.Add(valutaDate = new DateTimePicker()
                {
                    Location = new Point(lm + 118, 2 * line + tm),
                    Width = 100,
                    Format = DateTimePickerFormat.Short,
                });
            
            this.Controls.Add(new Label()
                {
                    Text = "Amount:",
                    Location = new Point(lm, 3 * line + tm + labelOffs),
                    Size = new Size(110, labelOffs),
                    TextAlign = ContentAlignment.BottomRight,
                });
            this.Controls.Add(tbAmount = new TextBox()
                {
                    Text = "0,00",
                    Location = new Point(lm + 118, 3 * line + tm),
                    Width = 100,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    TextAlign = HorizontalAlignment.Right,
                });
            this.Controls.Add(cbCurrency = new ComboBox()
                {
                    Location = new Point(lm + 228, 3 * line + tm),
                    Width = 70,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    DropDownStyle = ComboBoxStyle.DropDownList,
                    ForeColor = Color.Black,
                });
            cbCurrency.Items.Add("EUR");
            cbCurrency.SelectedIndex = 0;

            this.Controls.Add(new Label()
                {
                    Text = "Transfer reason:",
                    Location = new Point(lm + 308, 0 * line + tm + labelOffs),
                    Size = new Size(110, labelOffs),
                    TextAlign = ContentAlignment.BottomLeft,
                });
            this.Controls.Add(tbTransferReason = new TextBox()
                {
                    Multiline = true,
                    Location = new Point(lm + 308, 1 * line + tm),
                    Width = this.Width - lm - rm - 324,
                    Height = 3 * (line + tm),
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                });
            TabControl tbc = new TabControl()
                {
                    Location = new Point(0, 105),
                    Size = new Size(this.Width, this.Height - 80),
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom,
                };
            this.Controls.Add(tbc);
            MoneyTransfer.eTransferType typeVal = MoneyTransfer.eTransferType.Unassigned;

            if (mt != null)
            {
                try { valutaDate.Value = mt.ValutaDate; } catch(Exception ex) { MessageBox.Show(ex.Message); }
                tbAmount.Text = mt.AmountString;
                if (mt.Currency != "EUR")
                {
                    cbCurrency.Items.Add(mt.Currency);
                    cbCurrency.SelectedIndex = 1;
                }
                typeVal = mt.TransferType;
                tbTransferReason.Text = mt.TransferReason;
            }

            foreach (MoneyTransfer.eTransferType type in Enum.GetValues(typeof(MoneyTransfer.eTransferType)))
            {
                KeyValue kv = cbType.AddKV($"{type}", type);
                if (type == typeVal) cbType.SelectedItem = kv;
            }

            bool mtAll = (mtType == typeof(MoneyTransfer));
            if (mtAll || mtType == typeof(CashTransfer)) tbc.TabPages.Add(BuildCashTransferGui());
            if (mtAll || mtType == typeof(BankTransfer)) tbc.TabPages.Add(BuildBankTransferGui());
        }
        public frmMoneyTransfer(MoneyTransfer src, bool writeEnabled = true)
        {
            mt = src;
            if (mt == null) throw new NullReferenceException("frmMoneyTransfer: supplied MoneyTransfer is null.");

            mtType = mt.GetType();
            this.writeEnabled = writeEnabled;
            this.assignedTo = Program.members.Find(mt.MemberNumber);

            BuildMoneyTransferGui();
        }
        public frmMoneyTransfer(Type srcType = null, Member assignTo = null)
        {
            mtType = srcType ?? typeof(MoneyTransfer);
            this.writeEnabled = true;
            this.assignedTo = assignTo;

            BuildMoneyTransferGui();
        }
        private void btnCashOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            if (mt == null) mt = new CashTransfer();
            this.Close();
        }
        private void btnBankOK_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            if (mt == null) mt = new BankTransfer();
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            mt = null;
            this.Close();
        }
        public MoneyTransfer FillMoneyTransfer()
        {
            //At this point, mt cannot be zero because it's initialized by the OK buttons on every tab.
            //but, friendly as we are, we handle this anyway
            if (mt == null) throw new NullReferenceException("No MoneyTransfer was created. User didn't click OK");

            if (!mt.AssignFixed)
            {
                mt.MemberNumber = (uint)((KeyValue)cbAssigned.SelectedItem).Value;
                mt.TransferType = (MoneyTransfer.eTransferType)((KeyValue)cbType.SelectedItem).Value;
            }
            mt.ValutaDate = valutaDate.Value;
            mt.Amount = core.Utils.StringToInt64FP(tbAmount.Text);
            mt.Currency = cbCurrency.Text;
            mt.TransferReason = tbTransferReason.Text;

            if (mt.GetType() == typeof(CashTransfer))
            {
                CashTransfer ct = (CashTransfer)mt;
                ct.RecipeNumber = tbCashRecipeNumber.Text;
            }
            if (mt.GetType() == typeof(BankTransfer))
            {
                BankTransfer bt = (BankTransfer)mt;
                bt.AccountInCharge = tbBankAccountInCharge.Text;
                bt.BookingDate = bankBookingDate.Value;
                bt.BookingText = tbBankBookingText.Text;
                bt.CreditorID = tbBankCreditorID.Text;
                bt.MandateReference = tbBankMandateReference.Text;
                bt.ClientReference = tbBankClientReference.Text;
                bt.Sammlerreferenz = tbBankSammlerreferenz.Text;
                bt.LastschriftUrsprungsbetrag = tbBankLastschriftUrsprungsbetrag.Text;
                bt.AuslagenersatzRuecklastschrift = tbBankAuslagenersatzRuecklastschrift.Text;
                bt.RecipientOrDebitor = tbBankRecipientOrDebitor.Text;
                bt.IBAN = tbBankIBAN.Text;
                bt.BIC = tbBankBIC.Text;
                bt.Info = tbBankInfo.Text;

            }

            return mt;
        }
    }
}
