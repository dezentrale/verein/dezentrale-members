﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using dezentrale.model;

namespace dezentrale.view
{
    public class LvMv : CustomListView<Mv>
    {
        protected override List<ConfigLVDataHandler> DefaultColumns
        {   get
            { return new List<ConfigLVDataHandler>()
                {
                    new ConfigLVDataHandler()
                    {
                        Name = "status",
                        Display = "Status",
                        Width = 57, TextAlign = HorizontalAlignment.Right,
                        CustomToString = ( x => ((Mv)x).Status.ToString()),
                    },
                    new ConfigLVDataHandler()
                    {
                        Name = "date",
                        Display = "EventDate",
                        Width = 160,
                        CustomToString = ( x => (((Mv)x).EventDate.ToString()) ),
                    },
                };
        }   }

        public LvMv() : base(Program.config.MemberListColumns, LvMv_ColumnsChanged) { }

        private static void LvMv_ColumnsChanged(object sender, ColumnsChangedArgs e)
        {
            Console.WriteLine("LvMv_ColumnsChanged");
            Program.config.MvListColumns.Clear();
            foreach (ConfigLVDataHandler c in e.Columns) Program.config.MvListColumns.Add(new ConfigLVColumn(c));
            XmlData.SaveToFile(Program.ConfigFile, Program.config);
        }
    }
}