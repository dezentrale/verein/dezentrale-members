﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using dezentrale.core;
using dezentrale.model;
using dezentrale.model.money;

namespace dezentrale.view
{
    class DateTimePickerWithBg : DateTimePicker
    {
        const int WM_ERASEBKGND = 0x14;
        protected override void WndProc(ref System.Windows.Forms.Message m)
        {
            if (m.Msg == WM_ERASEBKGND)
            {
                using (var g = Graphics.FromHdc(m.WParam))
                {
                    using (var b = new SolidBrush(this.BackColor))
                    {
                        g.FillRectangle(b, ClientRectangle);
                    }
                }
                return;
            }

            base.WndProc(ref m);
        }
    }
    public class frmPaymentReceipts : FormWithActionButtons, IPaymentReceiptProcessData
    {
        public List<Member> MemberList       { get; set; } = null;
        public bool AllMembers               { get; set; } = false;
        public string DataTemplate           { get; set; } = "";
        public IntermediateFormat DataFormat { get; set; } = IntermediateFormat.Text;
        public string OutputDirectory        { get; set; } = "";
        public string FileNamePattern        { get; set; } = "{Number}-{Date}";
        public DateTime StartDate            { get; set; }
        public DateTime EndDate              { get; set; }
        public bool SendEmail                { get; set; } = false;
        public string StartDateString        { get { return StartDate.ToShortDateString(); } }
        public string EndDateString          { get { return EndDate  .ToShortDateString(); } }
        public bool WriteProtectPdf          { get; set; } = false;

        private TextBox tbTemplate;
        private ComboBox cbDataFormat;
        private TextBox tbOutputDirectory;
        private TextBox tbFileNamePattern;
        private DateTimePickerWithBg dtStartDate;
        private DateTimePickerWithBg dtEndDate;
        private RadioButton optAllMembers;
        private RadioButton optSelectedMembers;
        private CheckBox chkSendEmail;
        private CheckBox chkWriteProtectPdf;

        private Button btnOk;

        public frmPaymentReceipts()
        {
            btnOk = AddButton("OK", btnOK_Click);
            AddButton("Cancel", btnCancel_Click);

            DialogResult = DialogResult.Cancel;
            this.StartPosition = FormStartPosition.CenterParent;
            this.Size        = new System.Drawing.Size(483, 320);
            this.MinimumSize = new System.Drawing.Size(455, 320);
            this.Text = "Generate payment receipts";
            this.Controls.Add(new Label()
                {
                    Text = "Input template:",
                    Location = new Point(lm, 0 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            this.Controls.Add(tbTemplate = new TextBox()
                {
                    Location = new Point(lm + 113, 0 * line + tm),
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    Width = 300,
                });
            Button btnBrowseTemplate;
            this.Controls.Add(btnBrowseTemplate = new Button()
                {
                    Text = "...",
                    Location = new Point(lm + 417, 0 * line + tm),
                    Anchor = AnchorStyles.Top | AnchorStyles.Right,
                    Width = 40,
                });
            btnBrowseTemplate.Click += btnBrowseTemplate_Click;

            this.Controls.Add(new Label()
                {
                    Text = "Data format:",
                    Location = new Point(lm, 1 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            this.Controls.Add(cbDataFormat = new ComboBox()
                {
                    DropDownStyle = ComboBoxStyle.DropDownList,
                    Location = new Point(lm + 113, 1 * line + tm),
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    Width = 345,
                });
            foreach (IntermediateFormat fmt in Enum.GetValues(typeof(IntermediateFormat)))
                cbDataFormat.Items.Add(fmt);
            cbDataFormat.SelectedValueChanged += (sender, e) =>
                {
                    chkWriteProtectPdf.Enabled = (IntermediateFormat)cbDataFormat.SelectedItem != IntermediateFormat.Text;
                };

            this.Controls.Add(new Label()
                {
                    Text = "Output directory:",
                    Location = new Point(lm, 2 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            this.Controls.Add(tbOutputDirectory = new TextBox()
                {
                    Location = new Point(lm + 113, 2 * line + tm),
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    Width = 300,
                });
            Button btnBrowseOutputDir;
            this.Controls.Add(btnBrowseOutputDir = new Button()
                {
                    Text = "...",
                    Location = new Point(lm + 417, 2 * line + tm),
                    Anchor = AnchorStyles.Top | AnchorStyles.Right,
                    Width = 40,
                });
            btnBrowseOutputDir.Click += btnBrowseOutputDir_Click;

            this.Controls.Add(new Label()
                {
                    Text = "Filename pattern:",
                    Location = new Point(lm, 3 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            this.Controls.Add(tbFileNamePattern = new TextBox()
                {
                    Location = new Point(lm + 113, 3 * line + tm),
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    Width = 345,
                });

            
            this.Controls.Add(new Label()
                {
                    Text = "Start date:",
                    Location = new Point(lm, 4 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            this.Controls.Add(dtStartDate = new DateTimePickerWithBg()
                {
                    Location = new Point(lm + 113, 4 * line + tm),
                    Width = 100,
                    Format = DateTimePickerFormat.Short,
                });
            this.Controls.Add(new Label()
                {
                    Text = "00:00:00",
                    Location = new Point(lm + 160, 4 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            dtStartDate.ValueChanged += dt_Changed;


            this.Controls.Add(new Label()
                {
                    Text = "End date:",
                    Location = new Point(lm, 5 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            this.Controls.Add(dtEndDate = new DateTimePickerWithBg()
                {
                    Location = new Point(lm + 113, 5 * line + tm),
                    Width = 100,
                    Format = DateTimePickerFormat.Short,
                });
            this.Controls.Add(new Label()
                {
                    Text = "23:59:59",
                    Location = new Point(lm + 160, 5 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            dtEndDate.ValueChanged += dt_Changed;


            this.Controls.Add(optAllMembers = new RadioButton()
                {
                    Text = "Für alle Mitglieder durchführen",
                    Width = 300,
                    Location = new Point(lm + 113, 6 * line + tm),
                });
            this.Controls.Add(optSelectedMembers = new RadioButton()
                {
                    Text = "Für die ausgewählten Mitglieder durchführen",
                    Width = 300,
                    Location = new Point(lm + 113, 7 * line + tm),
                });
            this.Controls.Add(chkSendEmail = new CheckBox()
                {
                    Text = "Die erstellten Dateien per E-Mail senden",
                    Width = 300,
                    Location = new Point(lm + 113, 8 * line + tm),
                });
            this.Controls.Add(chkWriteProtectPdf = new CheckBox()
                {
                    Text = "Ausgabe schreibschützen (QPDF erforderlich)",
                    Width = 300,
                    Location = new Point(lm + 113, 9 * line + tm),
                });

            //We must run the filling of the data fields in the load event, as they
            //might have been changed after the constructor is executed
            this.Load += (sender, e) =>
            {
                tbTemplate.Text = DataTemplate;
                cbDataFormat.SelectedItem = DataFormat;
                tbOutputDirectory.Text = OutputDirectory;
                tbFileNamePattern.Text = FileNamePattern;
                dtStartDate.Value = StartDate;
                dtEndDate.Value = EndDate;
                if (MemberList != null && MemberList.Count > 0)
                {
                    optAllMembers.Checked      = AllMembers;
                    optSelectedMembers.Checked = !AllMembers;
                }
                else
                {
                    optAllMembers.Checked = true;
                    optSelectedMembers.Enabled = false;
                }
                chkSendEmail.Checked = SendEmail;
                chkWriteProtectPdf.Checked = WriteProtectPdf;
            };
        }

        private void btnBrowseTemplate_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            DialogResult dr = ofd.ShowDialog();
            if(dr == DialogResult.OK)
            {
                tbTemplate.Text = ofd.FileName;
            }
        }

        private void btnBrowseOutputDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult dr = fbd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                tbOutputDirectory.Text = fbd.SelectedPath;
            }
        }


        private void dt_Changed(object sender, EventArgs e)
        {
            if(dtStartDate.Value.CompareTo(dtEndDate.Value) > 0)
            {
                dtStartDate.BackColor = Color.LightYellow;
                dtEndDate.BackColor = Color.LightYellow;
                btnOk.Enabled = false;
            } else
            {
                dtStartDate.BackColor = Color.White;
                dtEndDate.BackColor = Color.White;
                btnOk.Enabled = true;
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            AllMembers = optAllMembers.Checked;
            DataTemplate = tbTemplate.Text;
            DataFormat = (IntermediateFormat) cbDataFormat.SelectedItem;
            OutputDirectory = tbOutputDirectory.Text;
            FileNamePattern = tbFileNamePattern.Text;
            StartDate = dtStartDate.Value;
            EndDate = dtEndDate.Value.AddSeconds(24*60*60 - 1);
            if (optSelectedMembers.Checked != true) MemberList = null;
            SendEmail = chkSendEmail.Checked;
            WriteProtectPdf = chkWriteProtectPdf.Checked;
            DialogResult = DialogResult.OK;
            this.Close();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}