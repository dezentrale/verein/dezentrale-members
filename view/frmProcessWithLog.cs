﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using dezentrale.core;
using dezentrale.model;

namespace dezentrale.view
{
    public class frmProcessWithLog : Form, IProcessController
    {
        private DialogResult toReturn = DialogResult.Cancel;
        private TextBox tbLog    = null;
        private Label lblStepStatus = null;
        private PictureBox picStatus  = null;
        private Button btnStart  = null;
        private Button btnCancel = null;
        private Button btnClose  = null;

        private BackgroundProcess process;
        public void Clear()
        {
            tbLog.Invoke(new Action(() => tbLog.Clear()));
            Console.Clear();
        }

        private void printline(string line)
        {
            tbLog.Invoke(new Action(() => 
                tbLog.AppendText(line + "\r\n")
                        ))
                ;
            Console.WriteLine(line);
        }
        public void LogRaw(string text)
        {
            printline(text);
        }


        public void LogLine(string text, LogEvent.ELogLevel logLevel, string module)
        {
            string output = $"{DateTime.Now}  [{module}]  {logLevel}  {text}";
            printline(output);
        }

        public frmProcessWithLog(BackgroundProcess process, bool autoRun = false)
        {
            this.process = process;
            this.process.LogTarget = this;
            this.Text = $"Process - {this.process.Caption} (waiting)";
            this.Width = 800;
            this.Height = 600;
            this.StartPosition = FormStartPosition.CenterParent;

            this.FormClosing += (sender, e) => { DialogResult = toReturn; };
            this.Load += (sender, e) => { if (autoRun) btnStart_Click(null, null); };

            // build GUI
            //[TextBox Multiline                  ]
            //[Step display label                 ]
            //[SuccessImg] [Start] [Cancel] [Close]

            tbLog = new TextBox()
            {
                Location = new System.Drawing.Point(5, 5),
                Size = new Size(this.Width - 15, this.Height - 95),
                Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom,
                Multiline = true,
                ScrollBars = ScrollBars.Both,
            };
            this.Controls.Add(tbLog);

            picStatus = new PictureBox()
            {
                Location = new System.Drawing.Point(5, this.Height - 80),
                Size = new Size(48, 48),
                Anchor = AnchorStyles.Left | AnchorStyles.Bottom,
                BackColor = Color.White,
            };
            this.Controls.Add(picStatus);

            lblStepStatus = new Label()
            {
                Location = new System.Drawing.Point(60, this.Height - 80),
                AutoSize = false,
                Size = new Size(this.Width - 10, 20),
                Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom,
                Text = "Ready.",
            };
            this.Controls.Add(lblStepStatus);

            btnStart = new Button()
            {
                Text = "Start",
                Location = new System.Drawing.Point(this.Width - 260, this.Height - 55),
                Anchor = AnchorStyles.Right | AnchorStyles.Bottom,
                Enabled = !autoRun,
            };
            btnStart.Height += 2;
            btnStart.Click += btnStart_Click;
            this.Controls.Add(btnStart);

            btnCancel = new Button()
            {
                Text = "Cancel",
                Location = new System.Drawing.Point(this.Width - 175, this.Height - 55),
                Anchor = AnchorStyles.Right | AnchorStyles.Bottom,
                Enabled = false,
            };
            btnCancel.Height += 2;
            btnCancel.Click += btnCancel_Click;
            this.Controls.Add(btnCancel);

            btnClose = new Button()
            {
                Text = "Close",
                Location = new System.Drawing.Point(this.Width - 90, this.Height - 55),
                Anchor = AnchorStyles.Right | AnchorStyles.Bottom,
                Enabled = true,
            };
            btnClose.Height += 2;
            btnClose.Click += btnClose_Click;
            this.Controls.Add(btnClose);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            btnCancel.Enabled = true;
            btnClose.Enabled = false;
            this.Text = $"Process - {this.process.Caption} (running)";

            process.StartRun();
        }
        private void btnCancel_Click(object sender, EventArgs e)
        {
            btnCancel.Enabled = false;
            this.Text = $"Process - {this.process.Caption} (cancelled)";

        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void StepStarted(uint stepNumber, string stepDescription)
        {
            lblStepStatus.Invoke(new Action(() => 
                lblStepStatus.Text = $"# {stepNumber + 1}/{this.process.Steps} : {stepDescription}"
                                ))
                ;
        }
        public void StepCompleted(uint stepNumber, string stepDescription, bool success)
        {
            lblStepStatus.Invoke(new Action(() => 
                lblStepStatus.Text = $"# {stepNumber + 1}/{this.process.Steps} : Completed({success}) {stepDescription}"
                                ))
                ;
        }
        public void ActionCompleted(bool success)
        {
            toReturn = success? DialogResult.Yes : DialogResult.No;
            btnCancel.Invoke(new Action(() =>
                btnCancel.Enabled = false
                            ))
                ;
            btnClose.Invoke(new Action(() => 
                btnClose.Enabled = true
                            ))
                ;
            picStatus.Invoke(new Action(() => 
                picStatus.BackColor = (success ? Color.LightGreen : Color.Red)
                            ))
                ;
            this.Invoke(new Action(() => 
                this.Text = $"Process - {this.process.Caption}" + (success ? " (done)" : " (failed)")
                            ))
                ;
        }
    }
}
