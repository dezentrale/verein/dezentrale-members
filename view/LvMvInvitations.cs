﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using dezentrale.model;

namespace dezentrale.view
{
    public class LvMvInvitations : CustomListView<MvInvitedMember>
    {
        protected override List<ConfigLVDataHandler> DefaultColumns
        {
            get
            {
                return new List<ConfigLVDataHandler>()
                {
                    new ConfigLVDataHandler()
                    {
                        Name = "number",
                        Display = "#",
                        Width = 40,
                        CustomToString =  x => ((MvInvitedMember)x).NumberString,
                    },
                    new ConfigLVDataHandler()
                    {
                        Name = "nickname",
                        Display = "name / nickname",
                        Width = 120,
                        CustomToString =  x => ((MvInvitedMember)x).Member?.Nickname,
                    },
                    new ConfigLVDataHandler()
                    {
                        Name = "inv",
                        Display = "Invited",
                        Width = 48, TextAlign = HorizontalAlignment.Right,
                        CustomToString = ( x => ((MvInvitedMember)x).InvitedString),
                    },
                    new ConfigLVDataHandler()
                    {
                        Name = "att",
                        Display = "Attended",
                        Width = 61, TextAlign = HorizontalAlignment.Right,
                        CustomToString = ( x => ((MvInvitedMember)x).AttendedString),
                    },
                    new ConfigLVDataHandler()
                    {
                        Name = "date",
                        Display = "InviteDate",
                        Width = 88,
                        CustomToString = ( x => (((MvInvitedMember)x).InvitationDate.Year > 1 ? ((MvInvitedMember)x).InvitationDate.ToString() : "-") ),
                    },
                };
            }
        }

        public LvMvInvitations() : base(Program.config.MvInvitationsListColumns, LvMvInvitations_ColumnsChanged) { }

        private static void LvMvInvitations_ColumnsChanged(object sender, ColumnsChangedArgs e)
        {
            Console.WriteLine("LvMvInvitations_ColumnsChanged");
            Program.config.MvInvitationsListColumns.Clear();
            foreach (ConfigLVDataHandler c in e.Columns) Program.config.MvInvitationsListColumns.Add(new ConfigLVColumn(c));
            XmlData.SaveToFile(Program.ConfigFile, Program.config);
        }
    }
}