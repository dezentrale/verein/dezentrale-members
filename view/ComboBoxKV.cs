﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using dezentrale.model;

namespace dezentrale.view
{
    public class ComboBoxKV : ComboBox
    {
        public KeyValue AddKV(string key, object value, object tag = null)
        {
            KeyValue kv = new KeyValue() { Key = key, Value = value, Tag = tag };
            this.Items.Add(kv);
            return kv;
        }
    }
}
