﻿using System;

namespace dezentrale.model
{
    public class KeyValue
    {
        public string Key   { get; set; }
        public object Value { get; set; }
        public object Tag   { get; set; }

        public override string ToString() { return Key; }
        public KeyValue()
        {
        }
    }
}
