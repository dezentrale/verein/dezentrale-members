﻿using System;

using dezentrale.model;

namespace dezentrale.core
{
    //! \short Logging interface for files, gui and processes.
    public interface ILogger
    {
        //! \short Clear a log window, if available
        void Clear();

        //! \short Log the string to the output, only appending a newline
        //! \param text     The informational payload, free-text. Can be multi-line too
        void LogRaw(string text);

        //! \short Log a formatted line using the given info from the parameters
        //! \param text     The informational payload, free-text. use one text line only
        //! \param logLevel Severity of the log event
        //! \param module   Current code unit, can be used to sort out debugging issues later
        void LogLine(string text, LogEvent.ELogLevel logLevel = LogEvent.ELogLevel.Info, string module = "");
    }
}
