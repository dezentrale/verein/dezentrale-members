﻿using System;
namespace dezentrale.core
{
    //! \short Interface for controlling processes and for getting more detailled information from them
    public interface IProcessController : ILogger
    {
        void ActionCompleted(bool success);
        void StepStarted(uint stepNumber = 0, string stepDescription = "Generic");
        void StepCompleted(uint stepNumber = 0, string stepDescription = "Generic", bool success = true);
        System.Windows.Forms.DialogResult DialogResult { get; set; }
    }
}
