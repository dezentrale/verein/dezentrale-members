﻿using System;
using System.Xml.Serialization;

namespace dezentrale.model.money
{
    [XmlType("CashTransfer")]
    public class CashTransfer : MoneyTransfer
    {
        [XmlElement] public string RecipeNumber { get; set; } = "";
        public CashTransfer() : base()
        {
            ValutaDate = DateTime.Today;
        }
    }
}
