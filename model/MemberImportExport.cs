﻿using System;
using System.IO;
using ICSharpCode.SharpZipLib.Zip;
using System.Xml.Serialization;
using System.Diagnostics;

namespace dezentrale.model
{
    public class MemberImportExport
    {
        [XmlElement] public string ZipFile { get; set; } = "fnord.zip";
        [XmlElement] public string ZipPassword { get; set; } = "";
        [XmlElement] public bool GpgEnabled { get; set; } = true;
        [XmlElement] public string GpgFile { get; set; } = "fnord.gpg";
        [XmlElement] public string GpgPassword { get; set; } = "fnord";
        [XmlElement] public bool HgEnabled { get; set; } = false;
        [XmlElement] public string HgUserName { get; set; } = "";
        [XmlElement] public string HgPassword { get; set; } = "";
        [XmlElement] public string HgURL { get; set; } = "";
        [XmlElement] public bool GitEnabled { get; set; } = false;
        [XmlElement] public string GitUserName { get; set; } = "";
        [XmlElement] public string GitPassword { get; set; } = "";

        private bool FilesCompare(string file1, string file2)
        {
            try
            {
                byte[] bytes1 = File.ReadAllBytes(file1);
                byte[] bytes2 = File.ReadAllBytes(file2);
                if (bytes1.Length != bytes2.Length)
                    throw new Exception($"Files differ in size: {bytes1.Length} vs {bytes2.Length}");
                for(int i = 0; i < bytes1.Length; i++)
                {
                    if(bytes1[i] != bytes2[i])
                        throw new Exception($"File contents differ at pos {i}: {bytes1[i]} vs {bytes2[i]}");
                }
            } catch(Exception ex)
            {
                Console.WriteLine($"Comparison error: {ex.Message}");
            }
            return true;
        }
    }
}
