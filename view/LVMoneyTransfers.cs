﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using dezentrale.model;
using dezentrale.model.money;

namespace dezentrale.view
{
    public class LVMoneyTransfers : Panel
    {
        class MT : CustomListView<MoneyTransfer>
        {
            protected override List<ConfigLVDataHandler> DefaultColumns
            {
                get
                {
                    return new List<ConfigLVDataHandler>()
                    {
                        new ConfigLVDataHandler()
                        {
                            Name = "id",
                            Display = "Id",
                            Width = 90,
                            CustomToString = ( x => ((MoneyTransfer)x).Id),
                        },
                        new ConfigLVDataHandler()
                        {
                            Name = "number",
                            Display = "Member",
                            Width = 57,
                            CustomToString = ( x => (((MoneyTransfer)x).MemberNumber > 0 ? $"{((MoneyTransfer)x).MemberNumber:D3}" : "-") ),
                        },
                        new ConfigLVDataHandler()
                        {
                            Name = "type",
                            Display = "Type",
                            Width = 130,
                            CustomToString = ( x => ($"{((MoneyTransfer)x).TransferType}") ),
                            //if (t.TransferType == MoneyTransfer.eTransferType.Unassigned) lvi.ForeColor = System.Drawing.Color.DarkRed;
                            //else                                                          lvi.ForeColor = System.Drawing.Color.Black;
                        },
                        new ConfigLVDataHandler()
                        {
                            Name = "date",
                            Display = "Date",
                            Width = 130,
                            CustomToString = ( x => (((MoneyTransfer)x).ValutaDate.ToString("yyyy-MM-dd")) ),
                        },
                        new ConfigLVDataHandler()
                        {
                            Name = "amount",
                            Display = "Amount",
                            Width = 70, TextAlign = HorizontalAlignment.Right,
                            CustomToString = ( x => $"{((MoneyTransfer)x).AmountString} {((MoneyTransfer)x).Currency}"),
                        },
                    };
                }
            }
            public MT() : base(Program.config.MTListColumns, LVMoneyTransfers_ColumnsChanged) { }
        }

        private MT mT = new MT() { Dock = DockStyle.Fill, };


        private Button btnLoadList = new Button()
        {
            Text = "Load money transfers from disk",
            Anchor = AnchorStyles.None,
            Size = new Size(250, 50),
        };

        public event EventHandler<LoadMoneyTransferArgs> LoadRequest;
        public LVMoneyTransfers()
        {
            btnLoadList.Location = new Point((this.ClientSize.Width - 250) / 2, (this.ClientSize.Height - 50) / 2);
            this.Controls.Add(btnLoadList);
            this.Controls.Add(mT);

            btnLoadList.Click += (sender, e) =>
            {
                LoadMoneyTransferArgs args = new LoadMoneyTransferArgs();
                LoadRequest?.Invoke(this, args);
                if (args.Cancel) return;
                if (args.Transfers != null)
                {
                    Console.WriteLine($"Loaded {args.Transfers.Count} Money transfers.");
                    LoadFromList(args.Transfers);
                } //else it might be handled this from outside, and just use the event handler on button press
            };

            this.AddMenuItem("&View transfer", MenuViewTransfer);
            this.AddMenuItem("&Edit transfer", MenuEditTransfer);
        }

        private void MenuViewTransfer(object sender, EventArgs e)
        {
            MoneyTransfer transfer = mT.GetFirstSelectedItem();
            if (transfer != null)
            {
                frmMoneyTransfer mtform = new frmMoneyTransfer(transfer, false);
                mtform.ShowDialog();
            }
        }
        private void MenuEditTransfer(object sender, EventArgs e)
        {
            MoneyTransfer transfer = mT.GetFirstSelectedItem();
            if (transfer != null)
            {
                uint mn = transfer.MemberNumber;
                frmMoneyTransfer mtform = new frmMoneyTransfer(transfer, true);
                DialogResult dr = mtform.ShowDialog();
                if(dr == DialogResult.OK)
                {
                    transfer = mtform.FillMoneyTransfer();

                    if(transfer.MemberNumber != mn)
                    {
                        Member m = Program.members.Find(transfer.MemberNumber);
                        if(m == null)
                        {
                            MessageBox.Show("Error while retrieving member #{transfer.MemberNumber}. No changes to transfer made.");
                            return;
                        }
                        m.StartLogEvent("Assigned bank transfer", LogEvent.eEventType.MembershipPayment, Program.config.LocalUser);
                        m.ApplyMoneyTransfer(transfer, Program.config.LocalUser);
                        transfer.AssignFixed = true;
                    }
                    Program.MoneyTransfers.SaveToFile();
                }
            }
        }

        public MenuItem AddMenuItem(string text, EventHandler handler = null, MenuItem parent = null, bool enabled = true) { return mT.AddMenuItem(text,handler,parent,enabled); }

        public void LoadFromList(List<MoneyTransfer> entries)
        {
            btnLoadList.Visible = false;
            mT.LoadFromList(entries);
        }
        public void ClearList()
        {

            btnLoadList.Visible = true;
            mT.Items.Clear();
        }
        private static void LVMoneyTransfers_ColumnsChanged(object sender, ColumnsChangedArgs e)
        {
            Console.WriteLine("LVMoneyTransfers_ColumnsChanged");
            Program.config.MTListColumns.Clear();
            foreach (ConfigLVDataHandler c in e.Columns) Program.config.MTListColumns.Add(new ConfigLVColumn(c));
            XmlData.SaveToFile(Program.ConfigFile, Program.config);
        }
    }
    public class LoadMoneyTransferArgs
    {
        public List<MoneyTransfer> Transfers { get; set; } = null;
        public bool Cancel { get; set; } = false;
    }
}
