﻿using System;
using System.Reflection;

namespace dezentrale.core
{
    public class ReplaceReflectEntity<T> where T : ReplaceReflectEntity<T> //Only child classes are allowed as T parameters
    {
        protected ReplaceReflectEntity()
        {

        }

        public T ReplaceReflect(object o)
        {
            T ret = (T) this.MemberwiseClone();

            PropertyInfo[] objProperties = o.GetType().GetProperties();

            foreach (var childProperty in ret.GetType().GetProperties())
            {
                if (childProperty.PropertyType != typeof(string)) continue;
                if (!childProperty.CanRead) continue;
                if (!childProperty.CanWrite) continue;

                string propVal = (string)childProperty.GetValue(ret, null);
                if (propVal == null) continue;
                bool changed = false;
                foreach (var objProperty in objProperties)
                {
                    if (!objProperty.CanRead) continue;

                    //check if objProperty occurs in mailProperty contents, then replace
                    string token = $"{{{objProperty.Name}}}";
                    string tokenValue;
                    try
                    {
                        tokenValue = objProperty.GetValue(o, null).ToString();
                    }
                    catch (NullReferenceException)
                    {
                        //Console.WriteLine($"FormMail.ReplaceReflect({o}): property {token}: {ex.Message}");
                        continue;
                    }

                    if (propVal.Contains(token))
                    {
                        //NOTE: This is problematic because it allows the user to generate recursive replacements by setting e.g. the Nickname to "{PgpFingerprint}"
                        propVal = propVal.Replace(token, tokenValue);
                        changed = true;
                    }
                }
                if (changed)
                {
                    childProperty.SetValue(ret, propVal, null);
                }
            }
            return ret;
        }
    }
}
