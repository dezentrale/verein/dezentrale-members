﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using dezentrale.model;
using dezentrale.model.money;

namespace dezentrale.core
{
    //! \brief Process for finishing a MV. This includes sending out multiple
    //!        E-Mails to the previously invited members, processing the
    //!        Foerdermitglied/regulaer state of the members etc.
    public class MvFinishProcess : BackgroundProcess
    {
        private Mv mv = null;

        public MvFinishProcess(Mv mv)
        {
            this.mv = mv;
            if (mv == null) throw new NullReferenceException("MV given to MvFinishProcess() is null");

            Caption = "Finish MV";
            Steps = (uint) (mv.Members.Count + 3);
        }

        //! \short  Run MV finish process
        //! \brief  For each invited member, check the attended status and
        //!         set the mv miss counter / Foerdermitglied/regular status.
        //!         Additionally, send an E-Mail with status changes or thanks
        //!         for attendance. Also, send a summary about Foerdermitglied/regular
        //!         to the Vorstand
        //! \return true if the process was successful.
        protected override bool Run()
        {
            uint step = 0;
            try
            {
                //Check mv
                if (mv.Status != Mv.MvStatus.Started)
                    throw new Exception($"MV is not in state {Mv.MvStatus.Started}");


                FormMail mvFinishedNotification   = FormMail.GenerateMvFinishedNotification();
                FormMail mvTypeChangeNotification = FormMail.GenerateMembershipTypeChanged();

                foreach (MvInvitedMember mvi in mv.Members)
                {
                    string entryDesc = $"{mvi.MemberNumber} ({mvi.Member.Nickname})";
                    LogTarget.StepStarted(++step, $"Processing entry {entryDesc}");

                    LogSubEvent lse = null;
                    if (mvi.Invited)
                    {
                        try
                        {
                            if (mvi.AttendedMv)
                            {
                                mvi.Member.StartLogEvent($"MV attended at {mv.EventDate}", LogEvent.eEventType.Generic);

                                //Optional: Send mail to thank that MV was attended
                                lse = mvFinishedNotification.Send(mvi.Member);
                                if (lse.Type == LogEvent.eEventType.Error) throw new Exception($"Sending mail to {entryDesc} failed.");
                                mvi.Member.MvMissCounter = 0;
                            }
                            else
                            {
                                mvi.Member.StartLogEvent($"MV missed at {mv.EventDate}", LogEvent.eEventType.Generic);
                                if (mvi.Member.Type == Member.eType.Regulaer)
                                {
                                    mvi.Member.MvMissCounter++;
                                    if (mvi.Member.MvMissCounter >= 2)
                                    {
                                        //Send mail that type changed to Foerdermitglied
                                        lse = mvTypeChangeNotification.Send(mvi.Member);
                                        if (lse.Type == LogEvent.eEventType.Error) throw new Exception($"Sending mail to {entryDesc} failed.");
                                        mvi.Member.Type = Member.eType.Foerdermitglied;
                                    }
                                    else
                                    {
                                        //Optional: Send mail that MV was missed
                                    }
                                }
                            }
                            if (lse != null) mvi.Member.CurrentLog.SubEvents.Add(lse);
                            mvi.Member.SaveToFile();
                            LogTarget.StepCompleted(step, $"Completed member entry {entryDesc}", true);
                        } catch(Exception ex)
                        {

                            if (lse == null) lse = new LogSubEvent() { Type = LogEvent.eEventType.Error, Topic = ex.Message, Details = ex.StackTrace };
                            mvi.Member.CurrentLog.SubEvents.Add(lse);
                            LogTarget.LogLine(ex.Message, LogEvent.ELogLevel.Error, "MvFinishProcess");
                            if(ex.InnerException != null)
                                LogTarget.LogLine(ex.InnerException.Message, LogEvent.ELogLevel.Error, "MvFinishProcess");

                            LogTarget.StepCompleted(step, $"Completed member entry {entryDesc}", false);
                        }
                    } else
                        LogTarget.StepCompleted(step, $"Not invited - skipped {entryDesc}", true);
                }

                mv.Status = Mv.MvStatus.Ended;
                mv.EndDateTime = DateTime.Now;
                //Send report to Vorstand
                return true;
            }
            catch (Exception ex)
            {
                LogTarget.LogLine($"An Error occurred: {ex.Message}", LogEvent.ELogLevel.Error, "MvFinishProcess");
                LogTarget.StepCompleted(step, $"MV finish", false);
                return false;
            }
        }
    }
}