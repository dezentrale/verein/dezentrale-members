﻿using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace dezentrale.view
{
    [Obsolete("Use FormWithActionButtons directly")]
    public class FormWithOkCancel : FormWithActionButtons
    {
        protected Button btnOk, btnCancel;

        protected void AddOkCancel(EventHandler okEvent, EventHandler cancelEvent)
        {
            btnOk = AddButton("OK", okEvent);
            btnCancel = AddButton("Cancel", cancelEvent);
        }
    }
}