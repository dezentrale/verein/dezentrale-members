﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using dezentrale.model;
using dezentrale.model.money;

namespace dezentrale.view
{
    public class frmEditEntry : Form
    {
        private Member member;
        private bool newMember = false;
        private bool eMailNameDefaults = true;

        private TextBox tbFirstName;
        private TextBox tbLastName;
        private DateTimePicker dateBirthday;
        private TextBox tbNickname;
        private TextBox tbStreet;
        private TextBox tbNumber;
        private TextBox tbZipcode;
        private TextBox tbCity;
        protected ComboBoxKV cbCountry;
        private TextBox tbEMailName;
        private TextBox tbEMail;
        private TextBox tbPgpFingerprint;
        private ComboBoxKV cbType;
        private DateTimePicker dateSpawn;
        private TextBox tbPaymentAmount;
        private CheckBox chkPaymentReduced;
        private ComboBoxKV cbRole;
        private TextBox tbMvMiss;
        private TextBox tbAccountBalance;
        private CheckBox cbEvaluateAccountInCharge;
        private TextBox tbBankAccountInCharge;
        private CheckBox cbPaymentNotification;
        private TextBox tbBankTransferRegEx;
        private TextBox tbRemarks;
        private DateTimePicker dateMemberForm;

        private ComboBoxKV cbStatus;
        private LogView lv;
        private LVMoneyTransfers mtv;

        private const int margin = 5;
        private const int lm = margin;   //Left margin
        private const int tm = margin;   //Top margin
        private const int rm = margin;   //Right margin
        private const int bm = margin;   //Bottom margin
        private const int line = 20 + margin;    //lineheight
        private const int labelHeight = 14;
        private const int labelOffs = 3;

        private void BuildMemberForm(Control parent)
        {
            int w = parent.ClientSize.Width - lm - rm;
            int h = parent.ClientSize.Height - tm - bm;

            SplitContainer sc = new SplitContainer()
            {
                Location = new Point(lm, tm),
                Size = new Size(w, 100),
                SplitterDistance = w / 2,
                Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                Margin = new Padding(margin),
            };
            parent.Controls.Add(sc);
            int w_pan1 = w / 2 - 10; //sc.Panel1.Width
            int w_pan2 = w / 2 - 10; //sc.Panel2.Width

            sc.Panel1.Controls.Add(new Label()
                {
                    Text = "First name:",
                    Location = new Point(0, 0 * line + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            sc.Panel1.Controls.Add(tbFirstName = new TextBox()
                {
                    Location = new Point(118, 0 * line),
                    Width = w_pan1 - 118,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                });
            sc.Panel1.Controls.Add(new Label()
                {
                    Text = "Last name:",
                    Location = new Point(0, 1 * line + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            sc.Panel1.Controls.Add(tbLastName = new TextBox()
                {
                    Location = new Point(118, 1 * line),
                    Width = w_pan1 - 118,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                });
            sc.Panel1.Controls.Add(new Label()
                {
                    Text = "Birthday:",
                    Location = new Point(0, 2 * line + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            sc.Panel1.Controls.Add(dateBirthday = new DateTimePicker()
                {
                    Location = new Point(118, 2 * line),
                    Width = 100,
                    Format = DateTimePickerFormat.Short,
                    //MinDate = new DateTime(1900, 01, 01, 00, 00, 00),
                    //MaxDate = DateTime.Today,
                });
            sc.Panel1.Controls.Add(new Label()
                {
                    Text = "Nickname:",
                    Location = new Point(0, 3 * line + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            sc.Panel1.Controls.Add(tbNickname = new TextBox()
                {
                    Location = new Point(118, 3 * line),
                    Width = w_pan1 - 118,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                });
            sc.Panel2.Controls.Add(new Label()
                {
                    Text = "Street, Number:",
                    Location = new Point(0, 0 * line + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            sc.Panel2.Controls.Add(tbStreet = new TextBox()
                {
                    Location = new Point(118, 0 * line),
                    Width = w_pan2 - 118 - 43,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                });
            sc.Panel2.Controls.Add(tbNumber = new TextBox()
                {
                    Location = new Point(118 + tbStreet.Width + 5, 0 * line),
                    Width = 40,
                    Anchor = AnchorStyles.Top | AnchorStyles.Right,
                });
            sc.Panel2.Controls.Add(new Label()
                {
                    Text = "Zipcode,City:",
                    Location = new Point(0, 1 * line + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            sc.Panel2.Controls.Add(tbZipcode = new TextBox()
                {
                    Location = new Point(118, 1 * line),
                    Width = 40,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            sc.Panel2.Controls.Add(tbCity = new TextBox()
                {
                    Location = new Point(118 + tbZipcode.Width + 5, 1 * line),
                    Width = w_pan2 - 118 - tbZipcode.Width - 3,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                });
            sc.Panel2.Controls.Add(new Label()
                {
                    Text = "Country:",
                    Location = new Point(0, 2 * line + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });

            sc.Panel2.Controls.Add(cbCountry = new ComboBoxKV()
                {
                    Location = new Point(118, 2 * line),
                    Width = w_pan2 - 118 + 2,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                    DropDownStyle = ComboBoxStyle.DropDownList,
                });
                
            parent.Controls.Add(new Label()
                {
                    Text = "EMail:",
                    Location = new Point(lm, 4 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(tbEMailName = new TextBox()
                {
                    Location = new Point(lm + 118, 4 * line + tm),
                    Width = 183,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            tbNickname.TextChanged += (sender, e) =>
                {
                    if(eMailNameDefaults)
                    {
                        tbEMailName.Text = tbNickname.Text;
                    }
                    else
                    if (tbEMailName.Text == tbNickname.Text)
                    {
                       eMailNameDefaults = true;
                        tbEMailName.ForeColor = System.Drawing.Color.DarkGray;
                    }
                };
            tbEMailName.TextChanged += (sender, e) =>
                {
                    if(eMailNameDefaults)
                    {

                        if (tbEMailName.Text != tbNickname.Text)
                        {
                            eMailNameDefaults = false;
                            tbEMailName.ForeColor = System.Drawing.Color.Black;
                        }
                    } else
                    if (tbEMailName.Text == tbNickname.Text)
                    {
                        if(!eMailNameDefaults)
                        {
                            eMailNameDefaults = true;
                            tbEMailName.ForeColor = System.Drawing.Color.DarkGray;
                        }
                    }
                };
            parent.Controls.Add(new Label()
                {
                    Text = "<",
                    Location = new Point(lm + 118 + 185, 4 * line + tm + labelOffs),
                    Size = new Size(12, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            parent.Controls.Add(new Label()
                {
                    Text = ">",
                    Location = new Point(w - 15, 4 * line + tm + labelOffs),
                    Size = new Size(12, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                    Anchor = AnchorStyles.Top | AnchorStyles.Right,
                });
            parent.Controls.Add(tbEMail = new TextBox()
                {
                    Location = new Point(lm + 118 + 205, 4 * line + tm),
                    Width = w - (lm + 118) - 222,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                });
            parent.Controls.Add(new Label()
                {
                    Text = "PGP Fingerprint:",
                    Location = new Point(lm, 5 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(tbPgpFingerprint = new TextBox()
                {
                    Location = new Point(lm + 118, 5 * line + tm),
                    Width = w - (lm + 118),
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right,
                });
            parent.Controls.Add(new Label()
                {
                    Text = "Membership type:",
                    Location = new Point(lm, 6 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(cbType = new ComboBoxKV()
                {
                    Location = new Point(lm + 118, 6 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    DropDownStyle = ComboBoxStyle.DropDownList,
                });
            parent.Controls.Add(new Label()
                {
                    Text = "Spawn Date:",
                    Location = new Point(lm, 7 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(dateSpawn = new DateTimePicker()
                {
                    Location = new Point(lm + 118, 7 * line + tm),
                    Width = 100,
                    Format = DateTimePickerFormat.Short,
                    //MinDate = new DateTime(1900, 01, 01, 00, 00, 00),
                    //MaxDate = DateTime.Today.AddYears(2),
                });
            parent.Controls.Add(new Label()
                {
                    Text = "Payment Amount:",
                    Location = new Point(lm, 8 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(tbPaymentAmount = new TextBox()
                {
                    Location = new Point(lm + 118, 8 * line + tm),
                    Width = 100,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    TextAlign = HorizontalAlignment.Right,
                });
            parent.Controls.Add(chkPaymentReduced = new CheckBox()
                {
                    Text = "Reduced fee (document needed)",
                    Location = new Point(lm + 118 + 100 + 15, 8 * line + tm),
                    Width = w - (lm + 118 + 100 + 15),
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            parent.Controls.Add(new Label()
                {
                    Text = "Signed Date:",
                    Location = new Point(lm, 9 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(dateMemberForm = new DateTimePicker()
                {
                    Location = new Point(lm + 118, 9 * line + tm),
                    Width = 100,
                    Format = DateTimePickerFormat.Short,
                    //MinDate = new DateTime(1900, 01, 01, 00, 00, 00),
                    //MaxDate = DateTime.Today,
                });
        }

        private void BuildMetadata(Control parent)
        {
            int w = parent.ClientSize.Width - lm - rm;
            int h = parent.ClientSize.Height - tm - bm;

            parent.Controls.Add(new Label()
                {
                    Text = "Account status:",
                    Location = new Point(lm, 0 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(cbStatus = new ComboBoxKV()
                {
                    Location = new Point(lm + 118, 0 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    DropDownStyle = ComboBoxStyle.DropDownList,
                });
            parent.Controls.Add(new Label()
                {
                    Text = "Member Role:",
                    Location = new Point(lm, 1 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(cbRole = new ComboBoxKV()
                {
                    Location = new Point(lm + 118, 1 * line + tm),
                    Width = 200,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    DropDownStyle = ComboBoxStyle.DropDownList,
                });
            parent.Controls.Add(new Label()
                {
                    Text = "Missed MVs:",
                    Location = new Point(lm, 2 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(tbMvMiss = new TextBox()
                {
                    Location = new Point(lm + 118, 2 * line + tm),
                    Width = 100,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    TextAlign = HorizontalAlignment.Right,
                });
            parent.Controls.Add(new Label()
                {
                    Text = "Account balance:",
                    Location = new Point(lm, 3 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(tbAccountBalance = new TextBox()
                {
                    Location = new Point(lm + 118, 3 * line + tm),
                    Width = 100,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                    TextAlign = HorizontalAlignment.Right,
                    ReadOnly = true,
                });
            parent.Controls.Add(new Label()
                {
                    Text = "AccountInCharge:",
                    Location = new Point(lm, 4 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(cbEvaluateAccountInCharge = new CheckBox()
                {
                    Location = new Point(lm + 118, 4 * line + tm),
                    Width = 20,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            parent.Controls.Add(tbBankAccountInCharge = new TextBox()
                {
                    Location = new Point(lm + 138, 4 * line + tm),
                    Width = 160,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            parent.Controls.Add(cbPaymentNotification = new CheckBox()
                {
                    Text = "Send payment notification via E-Mail",
                    Location = new Point(lm + 308, 4 * line + tm),
                    Width = 240,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });


            parent.Controls.Add(new Label()
                {
                    Text = "BankRegEx:",
                    Location = new Point(lm, 5 * line + tm + labelOffs),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomRight,
                });
            parent.Controls.Add(tbBankTransferRegEx = new TextBox()
                {
                    Location = new Point(lm + 118, 5 * line + tm),
                    Width = 160,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left,
                });
            cbEvaluateAccountInCharge.CheckedChanged += (sender, e) =>
            {
                tbBankAccountInCharge.ReadOnly = !cbEvaluateAccountInCharge.Checked;
                tbBankTransferRegEx.ReadOnly   = !cbEvaluateAccountInCharge.Checked;
            };

        /*
    [XmlElement] public DateTime GreetedDate        { get; set; }
    [XmlElement] public DateTime LastPaymentProcessed { get; set; }
    [XmlElement] public uint PaymentsTotal          { get; set; }
    [XmlElement] public DateTime LastBalanceDegrade { get; set; }
         */
            parent.Controls.Add(new Label()
                {
                    Text = "Remarks:",
                    Location = new Point(lm, 9 * line + tm),
                    Size = new Size(110, labelHeight),
                    TextAlign = ContentAlignment.BottomLeft,
                });
            parent.Controls.Add(tbRemarks = new TextBox()
                {
                    Location = new Point(lm + lm, 10 * line + tm),
                    Size = new Size(w - lm - rm, h - (10 * line + tm)),
                    Multiline = true,
                    ScrollBars = ScrollBars.Both,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom,
                });
        }
        private void BuildMoneyTransfers(Control parent)
        {
            parent.Controls.Add(mtv = new LVMoneyTransfers() { Dock = DockStyle.Fill, });
            if (member == null)
                mtv.LoadFromList(new List<MoneyTransfer>());
            else
            {
                if (Program.MoneyTransfersLoaded) mtv.LoadFromList(Program.MoneyTransfers.Entries.FindAll(x => x.MemberNumber == member.Number));
                else                              mtv.LoadRequest += (sender, e) => { e.Transfers = Program.MoneyTransfers.Entries.FindAll(x => x.MemberNumber == member.Number); };
            }
        }
        private void BuildLog(Control parent)
        {
            parent.Controls.Add(lv = new LogView(member.Log)
                {
                    Size = parent.ClientSize,
                    Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom,
                });
        }

        private void BuildGui(Control parent)
        {
            int w = parent.ClientSize.Width;
            int h = parent.ClientSize.Height - 35;
            int tabw = w - 5, tabh = h - 25;

            TabControl tc = new TabControl()
            {
                Size = new Size(w, h),
                Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Bottom,

                TabPages =
                {
                    new TabPage()
                    {
                        Text = "Member form",
                        ClientSize = new Size(tabw, tabh),
                    },
                    new TabPage()
                    {
                        Text = "Metadata",
                        ClientSize = new Size(tabw, tabh),
                    },
                    new TabPage()
                    {
                        Text = "Money transfers",
                        ClientSize = new Size(tabw, tabh),
                    },
                    new TabPage()
                    {
                        Text = "Log",
                        ClientSize = new Size(tabw, tabh),
                    }
                }
            };
            parent.Controls.Add(tc);
            BuildMemberForm(tc.TabPages[0]);
            BuildMetadata(tc.TabPages[1]);
            BuildMoneyTransfers(tc.TabPages[2]);
            BuildLog(tc.TabPages[3]);

            Button btnCancel = new Button() { Text = "Cancel", Size = new Size(80, 22), Anchor = AnchorStyles.Bottom | AnchorStyles.Right };
            Button btnOk = new Button() { Text = "OK", Size = new Size(80, 22), Anchor = AnchorStyles.Bottom | AnchorStyles.Right };
            btnCancel.Location = new Point(parent.ClientSize.Width - btnCancel.Width - 10, parent.ClientSize.Height - btnCancel.Height - 10);
            btnCancel.Click += BtnCancel_Click;

            btnOk.Location = new Point(parent.ClientSize.Width - btnOk.Width - btnCancel.Width - 16, parent.ClientSize.Height - btnOk.Height - 10);
            btnOk.Click += BtnOK_Click;

            parent.Controls.Add(btnCancel);
            parent.Controls.Add(btnOk);
        }

        public frmEditEntry(Member m, bool newMember = false)
        {

            member = m;
            this.newMember = newMember;

            this.SuspendLayout();
            this.StartPosition = FormStartPosition.CenterParent;
            //this.Size = new Size(640, 480);
            this.ClientSize = new Size(640, 480);
            this.MinimumSize = new Size(580, 360);
            BuildGui(this);

            //Fill form with data from m

            this.Text = newMember ? $"{member.Number:D3} - New member" : $"{member.Number:D3} ({member.Nickname}) - Edit member";
            tbFirstName.Text = member.FirstName;
            tbLastName.Text = member.LastName;
            try { dateBirthday.Value = member.Birthday; } catch (Exception)
            {
                if (!newMember)
                {
                    MessageBox.Show("Invalid birthday, " + member.Birthday + ", will be set to 1900-01-01");
                    dateBirthday.Value = new DateTime(1900, 01, 01);
                }
                else
                    dateBirthday.Value = DateTime.Today.AddYears(-30);
            }
            tbNickname.Text = member.Nickname;
            tbStreet.Text = member.Street;
            tbNumber.Text = member.HouseNumber;
            tbZipcode.Text = member.Zipcode;
            tbCity.Text = member.City;
            List<Country> cl = Country.GetList();
            foreach (Country c in cl)
            {
                string display = $"{c.A3} ({c.Code:d}) - {c.Name}";
                KeyValue kv = cbCountry.AddKV(display, c);
                if (c.Code == member.CountryCode) cbCountry.SelectedItem = kv;
            }

            eMailNameDefaults = m.EMailName == m.Nickname;
            tbEMail.Text = member.EMail;
            tbEMailName.Text = m.EMailName;
            if (eMailNameDefaults)
                tbEMailName.ForeColor = System.Drawing.Color.DarkGray;
            tbPgpFingerprint.Text = member.PgpFingerprint;

            foreach (Member.eType type in Enum.GetValues(typeof(Member.eType)))
            {
                KeyValue kv = cbType.AddKV($"{type}", type);
                if (type == member.Type) cbType.SelectedItem = kv;
            }
            try               { dateSpawn.Value = member.SpawnDate; }
            catch (Exception) { if (!newMember) { MessageBox.Show("Invalid SpawnDate: " + member.SpawnDate + "!"); } }
            tbPaymentAmount.Text = core.Utils.Int64FPToString((Int64) member.PaymentAmount);
            chkPaymentReduced.Checked = member.PaymentClass == Member.ePaymentClass.Reduced;
            try               { dateMemberForm.Value = member.MemberFormDate; }
            catch (Exception) { if (!newMember) { MessageBox.Show("Invalid MemberFormDate: " + member.MemberFormDate + "!"); } }

            foreach(Member.eStatus status in Enum.GetValues(typeof(Member.eStatus)))
            {
                KeyValue kv = cbStatus.AddKV($"{status}", status);
                if (status == member.Status) cbStatus.SelectedItem = kv;
            }
            foreach (Member.eRole role in Enum.GetValues(typeof(Member.eRole)))
            {
                KeyValue kv = cbRole.AddKV($"{role}", role);
                if (role == member.Role) cbRole.SelectedItem = kv;
            }

            tbMvMiss.Text = $"{member.MvMissCounter}";
            tbAccountBalance.Text = core.Utils.Int64FPToString(member.AccountBalance);
            cbEvaluateAccountInCharge.Checked = member.EvaluateAccountInCharge;
            tbBankAccountInCharge.ReadOnly = !cbEvaluateAccountInCharge.Checked;
            tbBankAccountInCharge.Text = member.BankAccountInCharge;
            cbPaymentNotification.Checked = member.PaymentNotify;
            tbBankTransferRegEx.ReadOnly   = !cbEvaluateAccountInCharge.Checked;
            tbBankTransferRegEx.Text = member.BankTransferRegEx;
            tbRemarks.Text = member.Remarks;
            this.ResumeLayout(false);
        }

        private void BtnOK_Click(object sender, EventArgs e)
        {
            //Sanity check

            //Generate log && copy over to object
            LogEvent evt = member.StartLogEvent(
                newMember ? $"Added user" : $"Edited user",
                newMember ? LogEvent.eEventType.AddUser : LogEvent.eEventType.EditUser);

            member.FirstName = tbFirstName.Text;
            member.FirstName = tbFirstName.Text;
            member.LastName = tbLastName.Text;
            member.Birthday = dateBirthday.Value;
            member.Nickname = tbNickname.Text;
            member.Street = tbStreet.Text;
            member.HouseNumber = tbNumber.Text;
            member.Zipcode = tbZipcode.Text;
            member.City = tbCity.Text;
            if (cbCountry.SelectedItem != null)
            {
                KeyValue kv = (KeyValue)cbCountry.SelectedItem;
                Country c = (Country)kv.Value;
                member.CountryCode = c.Code;
            }
            member.EMail = tbEMail.Text;
            member.EMailName = tbEMailName.Text;
            member.PgpFingerprint = tbPgpFingerprint.Text;
            if(cbType.SelectedItem != null)
            {
                KeyValue kv = (KeyValue)cbType.SelectedItem;
                member.Type = (Member.eType)kv.Value;
            }
            member.SpawnDate = dateSpawn.Value;
            if(newMember)
            {
                //For new members, we need to ensure correct payment handling.
                //This is done by defining that the member must pay its fee
                //for the month it wants to spawn. Therefore the last paid
                //month is the one before spawning. This simplifies things
                //for BalanceDegrade checks later.
                member.LastBalanceDegrade = member.SpawnDate.AddMonths(-1);
            }
            try
            {
                member.PaymentAmount = (UInt64) core.Utils.StringToInt64FP(tbPaymentAmount.Text);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot convert Payment Amount to fixed point value:\n{ex.Message}");
            }
            member.PaymentClass = chkPaymentReduced.Checked ? Member.ePaymentClass.Reduced : Member.ePaymentClass.Normal;
            member.MemberFormDate = dateMemberForm.Value;



            if(cbStatus.SelectedItem != null)
            {
                KeyValue kv = (KeyValue)cbStatus.SelectedItem;
                member.Status = (Member.eStatus)kv.Value;
            }

            if (cbRole.SelectedItem != null)
            {
                KeyValue kv = (KeyValue)cbRole.SelectedItem;
                member.Role = (Member.eRole)kv.Value;
            }
            member.MvMissCounter = Convert.ToUInt32(tbMvMiss.Text);
            try
            {
                member.AccountBalance = core.Utils.StringToInt64FP(tbAccountBalance.Text);//Convert.ToInt32(tbAccountBalance.Text);
            } catch(Exception ex)
            {
                Console.WriteLine($"Cannot convert Account balance to fixed point value:\n{ex.Message}");
            }
            member.EvaluateAccountInCharge = cbEvaluateAccountInCharge.Checked;
            member.BankAccountInCharge = tbBankAccountInCharge.Text;
            member.PaymentNotify = cbPaymentNotification.Checked;
            member.BankTransferRegEx = tbBankTransferRegEx.Text;
            member.Remarks = tbRemarks.Text;

            member.FinishLogEvent();
            this.DialogResult = DialogResult.OK;
            member.SaveToFile();

            this.Close();
        }
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
