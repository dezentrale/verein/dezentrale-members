﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace dezentrale.model.money
{
    [XmlType("BankTransfer")]
    public class BankTransfer : MoneyTransfer
    {
        //This resembles an 1:1 data image from CSV entries.
        [XmlElement] public string   AccountInCharge    { get; set; } = "";
        [XmlElement] public DateTime BookingDate        { get; set; }
        [XmlElement] public string   BookingText        { get; set; } = "";
        [XmlElement] public string   CreditorID         { get; set; } = "";
        [XmlElement] public string   MandateReference   { get; set; } = "";
        [XmlElement] public string   ClientReference    { get; set; } = "";

        [XmlElement] public string Sammlerreferenz                  { get; set; } = "";
        [XmlElement] public string LastschriftUrsprungsbetrag       { get; set; } = "";
        [XmlElement] public string AuslagenersatzRuecklastschrift   { get; set; } = "";

        [XmlElement] public string   RecipientOrDebitor { get; set; } = "";
        [XmlElement] public string   IBAN               { get; set; } = "";
        [XmlElement] public string   BIC                { get; set; } = "";
        [XmlElement] public string   Info               { get; set; } = "";

        public BankTransfer() : base() { }
        public BankTransfer(List<string> csvHeadline, List<string> csvEntry) : base()
        {
            int lc = csvEntry.Count;
            if (lc > csvHeadline.Count) lc = csvHeadline.Count;
            for (int i = 0; i < lc; i++)
            {
                switch (csvHeadline[i])
                {
                    case "Auftragskonto": AccountInCharge = csvEntry[i]; break;
                    case "Buchungstag": BookingDate = DateTime.Parse(csvEntry[i]); break;
                    case "Valutadatum": ValutaDate = DateTime.Parse(csvEntry[i]); break;
                    case "Buchungstext": BookingText = csvEntry[i]; break;
                    case "Verwendungszweck": TransferReason = csvEntry[i]; break;
                    case "Glaeubiger ID": CreditorID = csvEntry[i]; break;
                    case "Mandatsreferenz": MandateReference = csvEntry[i]; break;
                    case "Kundenreferenz (End-to-End)": ClientReference = csvEntry[i]; break;
                    case "Sammlerreferenz": Sammlerreferenz = csvEntry[i]; break;
                    case "Lastschrift Ursprungsbetrag": LastschriftUrsprungsbetrag = csvEntry[i]; break;
                    case "Auslagenersatz Ruecklastschrift": AuslagenersatzRuecklastschrift = csvEntry[i]; break;
                    case "Beguenstigter/Zahlungspflichtiger": RecipientOrDebitor = csvEntry[i]; break;
                    case "Kontonummer/IBAN":
                    case "Kontonummer": IBAN = csvEntry[i]; break;
                    case "BIC (SWIFT-Code)":
                    case "BLZ": BIC = csvEntry[i]; break;
                    case "Betrag":
                    {
                        string sAmount = csvEntry[i].Replace(',', '.');
                        float fAmount = float.Parse(sAmount, System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
                        Amount = Convert.ToInt64(fAmount * 100);
                    } break;
                    case "Waehrung": Currency = csvEntry[i]; break;
                    case "Info": Info = csvEntry[i]; break;
                    default:
                        throw new Exception($"invalid csv headline field: \"{csvHeadline[i]}\"");
                }
            }
        }
    }
}
