﻿using System;
using System.Collections.Generic;
using System.IO;

using dezentrale.model;
using dezentrale.model.money;

namespace dezentrale.core
{
    public class ProcessCsv
    {
        public static bool ProcessCSV(string fileName)
        {

            CsvFile csv = new CsvFile();
            csv.FieldSeparator = ';';
            try
            {
                List<BankTransfer> tmpList = new List<BankTransfer>();

                csv.ReadFile(fileName);
                List<string> headlineFields = null;
                List<Member> changedMembers = new List<Member>();
                foreach (List<string> l in csv.FileContents)
                {
                    if (headlineFields == null)
                    {
                        //The first line is expected to have the headline field first, describing the contents
                        headlineFields = l;
                        continue;
                    }

                    BankTransfer bt = new BankTransfer(headlineFields, l);


                    MoneyTransfer duplicate = Program.MoneyTransfers.FindEqualEntry(bt);
                    if (duplicate != null)
                    {
                        Console.WriteLine("Duplicate MoneyTransfer found");
                        Console.WriteLine($"ValutaDate: {bt.ValutaDate}, Amount = {bt.AmountString} {bt.Currency}, Reason = \"{bt.TransferReason.Replace('\r', '\\').Replace('\n', '\\')}\"");
                    }
                    else
                    {
                        Program.MoneyTransfers.AddEntry(bt);
                        tmpList.Add(bt);
                    }
                }

                //try to assign transfers to the members
                foreach (BankTransfer bt in tmpList)
                {
                    if (bt.Amount < 0)
                    {
                        bt.TransferType = MoneyTransfer.eTransferType.RunningCost;
                        Console.WriteLine($"{bt.Id}: Amount = {bt.AmountString} --> RunningCost");
                        continue;
                    }

                    foreach (Member m in Program.members.Entries)
                    {
                        if (m.CheckBankTransfer(bt))
                        {
                            bt.TransferType = MoneyTransfer.eTransferType.MembershipPayment;
                            changedMembers.Add(m);
                            m.StartLogEvent("Incoming bank transfer", LogEvent.eEventType.MembershipPayment, "automatic");
                            m.ApplyMoneyTransfer(bt);
                            bt.AssignFixed = true;
                            break; //this is important. We don't want to assign this to multiple members.
                        }
                    }
                }
                //Store bank transfer list
                Console.WriteLine("ProcessCSV(): Storing money transfers...");
                Program.MoneyTransfers.Entries.Sort();
                if (!Program.MoneyTransfers.SaveToFile())
                    return false;
                bool ret = true;
                /*
                //automaticly saved in ApplyMoneyTransfer()
                foreach (Member m in changedMembers)
                {
                    if (m.CurrentLog != null)
                    {
                        Console.Write($"ProcessCSV(): Storing {m.GetFileName()}... ");
                        try
                        {
                            if (!m.SaveToFile()) return false;
                            Console.WriteLine("OK");
                        } catch(Exception ex2)
                        {
                            Console.WriteLine($"ERROR: {ex2.Message}");
                            ret = false;
                        }
                    }
                }*/
                //TBD: mail to schatzmeister if there are unassigned transfers

                return ret;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error while processing csv file \"{fileName}\":");
                Console.WriteLine(ex.Message);
                return false;
            }
        }
    }
}
