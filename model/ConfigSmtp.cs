﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace dezentrale.model
{
    public class ConfigSmtp
    {
        [XmlElement] public bool   Enabled  { get; set; } = false;
        [XmlElement] public string Host     { get; set; } = "localhost";
        [XmlElement] public int    Port     { get; set; } = 587;
        [XmlElement] public bool   SSL      { get; set; } = true;
        [XmlElement] public string From     { get; set; } = "John Doe <john.doe@example.com>";
        [XmlElement] public string UserName { get; set; } = "username";      //you might need to use a complete e-mail address here.
        [XmlElement] public string Password { get; set; } = "password";
        [XmlElement] public string CcTo     { get; set; } = "John Doe <john.doe@example.com>";  //this helps to keep track on outgoing mails. use null or "" to disable.
    }
}
