﻿using System;
using System.Xml.Serialization;

namespace dezentrale.model
{
    public class ConfigLVColumn : IEquatable<ConfigLVColumn>
    {

        [XmlAttribute] public string Name    { get; set; } = "";
        [XmlAttribute] public bool   Visible { get; set; } = true;
        [XmlAttribute] public int    Width   { get; set; } = 80;


        /**\brief This is for resizing under mono: We need to store the width
         *        until the mouse button is released, to prevent an XML write
         *        for every pixel change.
         */
        [XmlIgnore]    public int    NewWidth{ get; set; } = 80;

        public ConfigLVColumn() : base() { }

        public ConfigLVColumn(ConfigLVColumn src)
        {
            if (src != null)
            {
                this.Name = src.Name;
                this.Visible = src.Visible;
                this.Width = src.Width;
            }
        }

        public bool Equals(ConfigLVColumn other)
        {
            return this.Name.Equals(other.Name);
        }
    }
}
