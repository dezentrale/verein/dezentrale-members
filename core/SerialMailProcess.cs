﻿using System;
using System.Collections.Generic;
using dezentrale.model;

namespace dezentrale.core
{
    public class SerialMailProcess<T> : BackgroundProcess
    {
        private FormMail fm = null;
        private List<T> entities = null;
        public List<T> EntitiesSuccessful { get; private set; } = new List<T>();
        public List<T> EntitiesFailed     { get; private set; } = new List<T>();

        private Func<FormMail, T, IProcessController, bool> customProcessor = null;

        private void Init(FormMail fmArg, List<T> entitiesArg)
        {
            this.fm = fmArg;
            this.entities = entitiesArg;

            Caption = "Send E-Mails";
            Steps = (uint)entities.Count; //number of members to contact
        }

        public SerialMailProcess(FormMail fm, List<T> entities)
        {
            Init(fm, entities);
        }
        public SerialMailProcess(FormMail fm, List<T> entities, Func<FormMail, T, IProcessController, bool> customProcessor)
        {
            this.customProcessor = customProcessor;
            Init(fm, entities);
        }


        //! \short  Run MV mail process
        //! \brief  For each selected member, an E-Mail is generated and
        //!         sent out, defined in @ref FormMail
        //! \return true if the generation was successful.
        protected override bool Run()
        {
            uint step = 0;

            string tType = typeof(T).ToString();

            foreach (T t in entities)
            {
                step++;
                try
                {
                    LogTarget.LogLine($"Sending mail for {tType} ({t.ToString()})...", LogEvent.ELogLevel.Info, "SerialMailProcess");
                    LogTarget.StepStarted(step, $"Sending mail for {tType} {t.ToString()}");

                    bool success = false;
                    if(customProcessor == null)
                    {
                        try
                        {
                            fm.Send(t);
                            success = true;
                        } catch(Exception ex)
                        {
                            LogTarget.LogLine($"{ex.Message}", LogEvent.ELogLevel.Error, "SerialMailProcess");
                            success = false;
                        }
                    } else
                    {
                        success = customProcessor(fm, t, LogTarget);
                    }
                    if (success)
                    {
                        EntitiesSuccessful.Add(t);
                    }
                    else
                    {
                        LogTarget.LogLine($"Custom mail process for {tType} {t.ToString()} returned false", LogEvent.ELogLevel.Error, "SerialMailProcess");
                        EntitiesFailed.Add(t);
                    }

                    LogTarget.StepCompleted(step, $"Done with {tType} {t.ToString()}", true);
                }
                catch (Exception ex)
                {
                    LogTarget.LogLine($"Error while processing {tType} {t.ToString()}: {ex.Message}", LogEvent.ELogLevel.Error, "SerialMailProcess");
                    LogTarget.StepCompleted(step, $"Error while processing {tType} {t.ToString()}", false);
                    break;
                }
            }

            LogTarget.LogLine($"Done.", LogEvent.ELogLevel.Info, "SerialMailProcess");
            return true;
        }
    }
}