﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace dezentrale.view
{
    //Todo: Transform into user control to be able to position & use it like a toolbar
    public class FormWithActionButtons : Form
    {
        protected const int margin = 5;
        protected const int lm = margin;   //Left margin
        protected const int tm = margin;   //Top margin
        protected const int rm = 20;   //Right margin
        protected const int line = 20 + margin;    //lineheight
        protected const int labelHeight = 14;
        protected const int labelOffs = 3;
        protected const int groupOffset = 10;

        protected List<Button> buttons = new List<Button>();

        //protected Button btnOk, btnCancel;
        protected Button AddButton(string text, EventHandler clickEvent, bool enabled = true)
        {
            //determine button width
            int buttonWidth = 80;

            //shift the old buttons to the left
            foreach(Button btn in buttons)
            {
                btn.Left -= buttonWidth + margin;
            }

            //add new button to the right
            Button b = new Button()
            {
                Text = text,
                Location = new System.Drawing.Point(this.Width - buttonWidth - rm, this.Height - 57),
                Width = buttonWidth,
                Anchor = AnchorStyles.Right | AnchorStyles.Bottom,
                Enabled = enabled,
            };
            buttons.Add(b);
            this.Controls.Add(b);
            if(clickEvent != null) b.Click += clickEvent;
            return b;
        }

        protected void ClearButtons()
        {
            foreach(Button b in buttons)
            {
                this.Controls.Remove(b);
            }
            buttons.Clear();
        }
    }
}