﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using dezentrale.model;

namespace dezentrale.view
{
    public class LogView : Panel
    {
        private List<LogEvent> evts = new List<LogEvent>();
        private ListView evtList;
        private ListView subEvtList;
        private TextBox tbDetails;

        public void BuildGui()
        {
            SplitContainer sc = new SplitContainer()
                {
                    Dock = DockStyle.Fill,
                    Orientation = Orientation.Horizontal,
                    Margin = new Padding(5),
                };
            this.Controls.Add(sc);

            SplitContainer sc2 = new SplitContainer()
                {
                    Dock = DockStyle.Fill,
                    Margin = new Padding(5),
                    SplitterDistance = this.Width/2,
                };

            sc.Panel1.Controls.Add(evtList = new ListView()
            {
                    Dock = DockStyle.Fill,
                    View = View.Details, //This gives us a traditional "list" view.
                    CheckBoxes = false,
                    FullRowSelect = true,
                    Columns =
                    {
                        new ColumnHeader() { Width = 0 },   //dummy
                        new ColumnHeader() { Text = "entries", Width = 45, TextAlign = HorizontalAlignment.Center },
                        new ColumnHeader() { Text = "timestamp", Width = 110 },
                        new ColumnHeader() { Text = "user", Width = 80 },
                        new ColumnHeader() { Text = "type", Width = 80 },
                        new ColumnHeader() { Text = "topic", Width = 220 },
                    },
                });
            sc.Panel2.Controls.Add(sc2);

            sc2.Panel1.Controls.Add(subEvtList = new ListView()
                {
                    Dock = DockStyle.Fill,
                    View = View.Details, //This gives us a traditional "list" view.
                    CheckBoxes = false,
                    FullRowSelect = true,
                    Enabled = false,
                    Columns =
                        {
                            new ColumnHeader() { Width = 0 },   //dummy
                            new ColumnHeader() { Text = "type", Width = 80 },
                            new ColumnHeader() { Text = "topic", Width = 220 },
                        },
                });
            sc2.Panel2.Controls.Add(tbDetails = new TextBox()
                {
                    Multiline = true,
                    Dock = DockStyle.Fill,
                    ScrollBars = ScrollBars.Both,
                    ReadOnly = true
                });

            evtList.SelectedIndexChanged += EvtList_SelectedIndexChanged;
            subEvtList.SelectedIndexChanged += SubEvtList_SelectedIndexChanged;
        }
        private string GetFirstLine(string input)
        {
            if (input == null) return "";
            if (input.Contains("\r")) input = input.Substring(0, input.IndexOf("\r") - 1);
            if (input.Contains("\n")) input = input.Substring(0, input.IndexOf("\n") - 1);
            return input;
        }

        private void EvtList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection sel = evtList.SelectedItems;
            if(sel.Count > 0)
            {
                ListViewItem lvi = sel[0];
                LogEvent le = (LogEvent) lvi.Tag;
                subEvtList.SuspendLayout();
                subEvtList.Items.Clear();
                foreach (LogSubEvent se in le.SubEvents)
                {
                    subEvtList.Items.Add(new ListViewItem()
                    {
                        Text = ".",
                        Tag = se,
                        SubItems =
                        {
                            new ListViewItem.ListViewSubItem() { Text = GetFirstLine($"{se.Type}") },
                            new ListViewItem.ListViewSubItem() { Text = GetFirstLine($"{se.Topic}") },
                        },
                    });
                }
                subEvtList.Enabled = le.SubEvents.Count > 0;
                subEvtList.ResumeLayout(true);
                ShowDetails(le);
            } else
            {
                ShowDetails(null);
                subEvtList.Items.Clear();
                subEvtList.Enabled = false;
            }
        }
        private void ShowDetails(LogSubEvent se)
        {
            if(se == null)
            {
                tbDetails.Enabled = false;
                tbDetails.Text = "";
            } else
            {
                tbDetails.Enabled = true;
                tbDetails.Text = $"Topic:\r\n  {se.Topic}\r\nType:\r\n  {se.Type}\r\n\r\nDetails:\r\n  {se.Details.Replace("\r\n", "\n").Replace("\r", "\n").Replace("\n", "\r\n  ")}";
            }
        }
        private void SubEvtList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection sel = subEvtList.SelectedItems;
            if (sel.Count > 0)
            {
                ListViewItem lvi = sel[0];
                LogSubEvent se = (LogSubEvent)lvi.Tag;
                ShowDetails(se);
            } else
            {
                ShowDetails(null);
            }
        }

        private void FillData(List<LogEvent> evts)
        {
            evtList.SelectedItems.Clear();
            foreach(LogEvent evt in evts)
            {
                evtList.Items.Add(new ListViewItem()
                    {
                        Tag = evt,
                        SubItems =
                        {
                            new ListViewItem.ListViewSubItem() { Text = $"{evt.SubEvents.Count}" },
                            new ListViewItem.ListViewSubItem() { Text = $"{evt.Timestamp.ToString("yyyy-MM-dd HH:mm")}" },
                            new ListViewItem.ListViewSubItem() { Text = GetFirstLine($"{evt.LocalUser}") },
                            new ListViewItem.ListViewSubItem() { Text = GetFirstLine($"{evt.Type}") },
                            new ListViewItem.ListViewSubItem() { Text = GetFirstLine($"{evt.Topic}") },
                        },
                    });
            }
        }
        public LogView(List<LogEvent> evts) : base()
        {
            this.evts = evts ?? throw new Exception("evts is null");

            BuildGui();
            FillData(this.evts);
        }

        public void Reload(List<LogEvent> evts = null)
        {
            if (evts != null) this.evts = evts;
            evtList.Clear();
            FillData(this.evts);
        }


    }
}
