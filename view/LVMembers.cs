﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

using dezentrale.model;

namespace dezentrale.view
{
    public class LVMembers : CustomListView<Member>
    {
        protected override List<ConfigLVDataHandler> DefaultColumns
        {   get
            { return new List<ConfigLVDataHandler>()
                {
                    new ConfigLVDataHandler()
                    {
                        Name = "number",
                        Display = "Member",
                        Width = 57, TextAlign = HorizontalAlignment.Right,
                        CustomToString = ( x => ((Member)x).NumberString),
                    },
                    new ConfigLVDataHandler()
                    {
                        Name = "nickname",
                        Display = "name / nickname",
                        Width = 160,
                        CustomToString = ( x => (((Member)x).Nickname ?? "{((Member)x).FirstName} {((Member)x).LastName}") ),
                    },
                    new ConfigLVDataHandler()
                    {
                        Name = "type",
                        Display = "type",
                        Width = 40,
                        CustomToString = ( x => (((Member)x).Type == Member.eType.Foerdermitglied ? "F" : "R") ),
                    },
                    new ConfigLVDataHandler()
                    {
                        Name = "status",
                        Display = "status",
                        Width = 100,

                        CustomToLvsi = ((lvi, o) =>
                        {
                            ListViewItem.ListViewSubItem lvsi = new ListViewItem.ListViewSubItem(lvi, $"{((Member)o).Status}");
                            if (((Member)o).Status == Member.eStatus.Disabled) lvi.ForeColor = Color.DarkGray;

                            return lvsi;
                        }),
                    },
                    new ConfigLVDataHandler()
                    {
                        Name = "mail",
                        Display = "mail",
                        Width = 40,
                        CustomToString = ( x => ((Member)x).EMail.Length < 1 ? "" : (((Member)x).PgpFingerprint.Length < 1 ? "yes" : "pgp") ),
                    },
                    new ConfigLVDataHandler()
                    {
                        Name = "balance",
                        Display = "balance",
                        Visible = false,
                        Width = 80, TextAlign = HorizontalAlignment.Right,
                        CustomToString = ( x => $"{((Member)x).AccountBalanceString}" ),
                    },
                };
        }   }

        public LVMembers() : base(Program.config.MemberListColumns, LVMembers_ColumnsChanged, true) { }

        private static void LVMembers_ColumnsChanged(object sender, ColumnsChangedArgs e)
        {
            Console.WriteLine("LVMembers_ColumnsChanged");
            Program.config.MemberListColumns.Clear();
            foreach (ConfigLVDataHandler c in e.Columns) Program.config.MemberListColumns.Add(new ConfigLVColumn(c));
            XmlData.SaveToFile(Program.ConfigFile, Program.config);
        }
    }
}
