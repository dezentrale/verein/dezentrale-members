﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace dezentrale.model
{
    public class MemberList
    {
        public List<Member> Entries { get; set; } = new List<Member>();

        public uint GetFreeNumber()
        {
            //Note: This assumes a sorted list in Entries.
            if (Entries.Count < 1) return 1;
            //uint ret =
            return  Entries[Entries.Count - 1].Number + 1;
            //return ret > ret2 ? ret : ret2;
        }
        public Member CreateMember(bool addToList = false)
        {
            Member m = new Member(GetFreeNumber());
            if(addToList) Entries.Add(m);
            return m;
        }
        public Member Find(uint number)
        {
            return Entries.Find(x => x.Number == number);
        }
        public Member Find(Member.eRole role)
        {
            return Entries.Find(x => x.Role == role);
        }
        public bool Remove(Member m)
        {
            if (m == null) return false;
            if (m.Role != Member.eRole.Normal)
                return false; //throw new Exception("Role is Vorstand!");
            m.Status = Member.eStatus.Deleted;
            return true;
        }


        public static MemberList LoadFromFiles(string[] memberFiles)
        {
            MemberList ml = new MemberList();
            foreach (string file in memberFiles)
            {
                ml.Entries.Add((Member)XmlData.LoadFromFile(file, typeof(Member)));
            }
            ml.Entries.Sort();
            return ml;
        }

        public static bool SaveToFiles(List<Member> entries = null, bool changedOnly = false)
        {
            if (entries == null) entries = Program.members.Entries;
            foreach (Member m in entries)
            {
                if (!m.SaveToFile()) return false;
            }
            return true;
        }

#if DEBUG
        public void GenerateTestData()
        {
            Entries.Clear();
            Member m;
            try
            {
                //the brackets are unnecessary but keep everything tidy.
                m = CreateMember(true);
                {
                    m.SuppressLogging = true;
                    m.Role = Member.eRole.Schatzmeister;
                    m.Type = Member.eType.Regulaer;
                    m.Status = Member.eStatus.Active;
                    //OpenPayments
                    //Log
                    m.Remarks = "riecht nach Knoblauch";
                    m.MvMissCounter = 0;
                    m.Nickname = "DoeJohnz";
                    m.FirstName = "John";
                    m.LastName = "Doe";
                    m.Street = "Homestreet";
                    m.HouseNumber = "123";
                    m.Zipcode = "12345";
                    m.City = "Musterstadt";
                    m.CountryCode = Country.eCountry.Germany;
                    m.Birthday = new DateTime(1980, 1, 1);
                    m.EMail = "john.doe@example.com";
                    m.PgpFingerprint = "";
                    m.MvInvitationByPost = false;
                    m.SpawnDate = new DateTime(2019, 6, 1);
                    m.PaymentClass = Member.ePaymentClass.Reduced;
                    m.PaymentAmount = 995;
                    m.MemberFormDate = new DateTime(2019, 5, 1);
                    m.SuppressLogging = false;
                }

                m = CreateMember(true);
                {
                    m.SuppressLogging = true;
                    m.Role = Member.eRole.Vorstandsvorsitzender;
                    m.Type = Member.eType.Regulaer;
                    m.Status = Member.eStatus.Active;
                    m.Remarks = "";
                    m.MvMissCounter = 0;
                    m.Nickname = "putin";
                    m.FirstName = "Vladimir";
                    m.LastName = "Putin";
                    m.Street = "Am Kreml";
                    m.HouseNumber = "1a";
                    m.Zipcode = "12345";
                    m.City = "Moskau";
                    m.CountryCode = Country.eCountry.RussianFederation;
                    m.Birthday = new DateTime(1960, 12, 24);
                    m.EMail = "h4x0r@kgb.ru@example.com";
                    m.PgpFingerprint = "encrypted ;-)";
                    m.MvInvitationByPost = false;
                    m.SpawnDate = new DateTime(2019, 6, 1);
                    m.PaymentClass = Member.ePaymentClass.Normal;
                    m.PaymentAmount = 6400;
                    m.MemberFormDate = new DateTime(2019, 5, 1);
                    m.SuppressLogging = false;
                }

                m = CreateMember(true);
                {
                    m.SuppressLogging = true;
                    m.Role = Member.eRole.Normal;
                    m.Type = Member.eType.Regulaer;
                    m.Status = Member.eStatus.Uninitialized;
                    m.Remarks = "";
                    m.MvMissCounter = 0;
                    m.Nickname = "AW23";
                    m.FirstName = "Adam";
                    m.LastName = "Weishaupt";
                    m.Street = "Parkallee";
                    m.HouseNumber = "13";
                    m.Zipcode = "99867";
                    m.City = "Gotha";
                    m.CountryCode = Country.eCountry.Germany;
                    m.Birthday = new DateTime(1900, 1, 1);// = new DateTime(1748, 2, 6);
                    m.EMail = "";
                    m.PgpFingerprint = "";
                    m.MvInvitationByPost = false;
                    m.SpawnDate = new DateTime(2019, 5, 23);
                    m.PaymentClass = Member.ePaymentClass.Normal;
                    m.PaymentAmount = 2305;
                    m.MemberFormDate = new DateTime(2019, 5, 23);
                    m.SuppressLogging = false;
                }

                m = CreateMember(true);
                {
                    m.SuppressLogging = true;
                    m.Role = Member.eRole.Normal;
                    m.Type = Member.eType.Regulaer;
                    m.Status = Member.eStatus.Greeted;
                    m.Remarks = "";
                    m.MvMissCounter = 0;
                    m.Nickname = "HeinzQuetschab";
                    m.FirstName = "Heinz";
                    m.LastName = "Ketchup";
                    m.Street = "";
                    m.HouseNumber = "";
                    m.Zipcode = "";
                    m.City = "Pittsburgh";
                    m.CountryCode = Country.eCountry.UnitedStatesOfAmerica;
                    m.Birthday = new DateTime(1900, 1, 1);// = new DateTime(1876, 12, 31);
                    m.EMail = "quetschab@example.com";
                    m.PgpFingerprint = "";
                    m.MvInvitationByPost = false;
                    m.SpawnDate = new DateTime(2019, 5, 1);
                    m.PaymentClass = Member.ePaymentClass.Normal;
                    m.PaymentAmount = 3200;
                    m.MemberFormDate = new DateTime(2019, 5, 1);
                    m.SuppressLogging = false;
                }

                m = CreateMember(true);
                {
                    m.SuppressLogging = true;
                    m.Role = Member.eRole.Normal;
                    m.Type = Member.eType.Regulaer;
                    m.Remarks = "";
                    m.MvMissCounter = 1;
                    m.Nickname = "Joshua";
                    m.FirstName = "Keiner";
                    m.LastName = "Niemand";
                    m.Street = "str.";
                    m.HouseNumber = "1";
                    m.Zipcode = "1";
                    m.City = "Musterstadt";
                    m.CountryCode = Country.eCountry.Germany;
                    m.Birthday = new DateTime(1980, 1, 1);
                    m.EMail = "joshua@wopr.gov@example.com";
                    m.PgpFingerprint = "";
                    m.MvInvitationByPost = false;
                    m.SpawnDate = new DateTime(2019, 5, 1);
                    m.PaymentClass = Member.ePaymentClass.Normal;
                    m.PaymentAmount = 3200;
                    m.MemberFormDate = new DateTime(2019, 5, 1);
                    m.SuppressLogging = false;
                }
                SaveToFiles();
            } catch(Exception ex)
            {
                throw ex;
            }
        }
#endif
    }
}
