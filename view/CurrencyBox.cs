﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace dezentrale.view
{
    public class CurrencyBox : Panel
    {
        public Int64 Value
        {
            get
            {
                return Convert.ToInt64(intPart.Text) * 100 + Convert.ToInt64(decimals.Text);
            }
            set
            {
                //todo: this might be wrong for negative values
                intPart.Text = $"{(Int64) (value / 100)}";
                decimals.Text = $"{(Int64) (value % 100)}";
            }
        }


        private TextBox intPart;
        private TextBox decimals;

        public override Color BackColor
        {
            get { return base.BackColor;  }
            set { base.BackColor = value; }
        }
        public CurrencyBox()
        {
            this.MinimumSize = new Size(100,16);
            this.Controls.Add(intPart = new TextBox()
                {
                    Width = this.Width - 30,
                    Anchor = AnchorStyles.Left | AnchorStyles.Right,
                    Text = "0"
                });
            intPart.TextChanged += IntPart_TextChanged;

            this.Controls.Add(decimals = new TextBox()
                {
                    Left = 75,
                    Width = 25,
                    Anchor = AnchorStyles.Right,
                    Text = "00"
                });
        }

        private void IntPart_TextChanged(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
