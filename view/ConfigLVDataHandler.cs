﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

using dezentrale.model;

namespace dezentrale.view
{
    public class ConfigLVDataHandler : ConfigLVColumn
    {
        [XmlIgnore] public Type DataType        { get; set; } = typeof(string);
        [XmlIgnore] public bool FilterAvailable { get; set; } = false;
        [XmlIgnore] public string Display       { get; set; } = "";
        [XmlIgnore] public System.Windows.Forms.HorizontalAlignment TextAlign { get; set; } = System.Windows.Forms.HorizontalAlignment.Left;

        public delegate ListViewItem.ListViewSubItem ObjToLvsi(ListViewItem lvi, object input);
        public delegate string ObjToString(object input);


        //Delegates to be overwritten for custom data presentation.
        //You have the option to overwrite the string generation and/or the generation of the ListViewSubItem itself (for custom lvsi settings, e.g. color dependent on data).
        [XmlIgnore] public ObjToLvsi CustomToLvsi { get; set; } = null;
        [XmlIgnore] public ObjToString CustomToString { get; set; } = (input => (input ?? "").ToString());

        //Initialize CustomToLvsi with default handler to generate ListViewSubItems
        public ConfigLVDataHandler(): this(null, null) { }

        public ConfigLVDataHandler(ConfigLVColumn src, ConfigLVDataHandler def) : base(src)
        {
            CustomToLvsi = CustomToLvsi ?? ((lvi, o) => new ListViewItem.ListViewSubItem(lvi, CustomToString(o)));
            if (def != null)
            {
                this.DataType = def.DataType;
                this.FilterAvailable = def.FilterAvailable;
                this.Display = def.Display;
                this.TextAlign = def.TextAlign;
                this.CustomToLvsi = def.CustomToLvsi;
                this.CustomToString = def.CustomToString;
            }
        }
    }
}
