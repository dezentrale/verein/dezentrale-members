﻿using System;
using dezentrale.model;

namespace dezentrale.view
{
    public class ConsoleLogger : core.IProcessController
    {
        public System.Windows.Forms.DialogResult DialogResult { get; set; } = System.Windows.Forms.DialogResult.Cancel;
        public ConsoleLogger()
        {

        }
        public System.Threading.Thread StartRunProcess(core.BackgroundProcess process)
        {
            Console.WriteLine($"Process - {process.Caption} (running)");

            System.Threading.Thread t = process.StartRun();
            if(t == null)
            {
                Console.WriteLine($"Process - {process.Caption} (starting error)");
            }
            return t;
        }
        public void Clear()
        {
            Console.Clear();
        }

        public void LogLine(string text, LogEvent.ELogLevel logLevel = LogEvent.ELogLevel.Info, string module = "")
        {
            Console.WriteLine($"{DateTime.Now}  [{module}]  {logLevel}  {text}");
        }

        public void LogRaw(string text)
        {
            Console.WriteLine(text);
        }

        public void StepStarted(uint stepNumber = 0, string stepDescription = "Generic")
        {
            Console.WriteLine($"# {stepNumber + 1}: Started {stepDescription}");
        }

        public void StepCompleted(uint stepNumber = 0, string stepDescription = "Generic", bool success = true)
        {
            Console.WriteLine($"# {stepNumber + 1}: Completed({success}) {stepDescription}");
        }
        public void ActionCompleted(bool success)
        {
            if (success) Console.WriteLine($"Process - done (OK)");
            else         Console.WriteLine($"Process - done (ERROR)");
        }
    }
}
