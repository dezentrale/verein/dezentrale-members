﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace dezentrale.model
{
    public class MvList : XmlData
    {
        [XmlElement("Mv")] public List<Mv> Entries = new List<Mv>();
        [XmlIgnore] public static string FileName { get; } = "mv.xml";

        public MvList()
        {
        }

        public static MvList LoadFromFile()
        {
            try
            {
                string mtlFile = System.IO.Path.Combine(Program.config.DbDirectory, FileName);
                return (MvList)XmlData.LoadFromFile(mtlFile, typeof(MvList));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error while loading MoneyTransferList: {ex.Message}");
                return new MvList();
            }
        }
        public static bool SaveToFile(MvList list, bool setDbChangedStatus = true)
        {
            string mvFile = System.IO.Path.Combine(Program.config.DbDirectory, FileName);
            if (setDbChangedStatus)
            {
                Program.config.DbChangedSinceExport = true;
                Program.config.LastDbLocalChange = DateTime.Now;
                XmlData.SaveToFile(Program.ConfigFile, Program.config);
            }
            return XmlData.SaveToFile(mvFile, list);
        }
        public bool SaveToFile(bool setDbChangedStatus = true)
        {
            return SaveToFile(this, setDbChangedStatus);
        }
    }
}
