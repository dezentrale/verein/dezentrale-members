﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace dezentrale.model
{
    public class ConfigVSMail
    {
        [XmlElement] public string VSName  { get; set; } = "dezentrale Vorstand";
        [XmlElement] public string VSEmail { get; set; } = "vorstand@dezentrale.space";
    }
}
