﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace dezentrale.model
{
    public class LogSubEvent
    {
        [XmlAttribute] public LogEvent.eEventType Type { get; set; } = LogEvent.eEventType.Generic;
        [XmlElement] public string Topic   { get; set; } = "";
        [XmlElement] public string Details { get; set; } = "";

    }
}
