﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace dezentrale.model
{
    public class XmlLog
    {
        public static                   bool SuppressAllLogging { get; set; } = false;

        [XmlIgnore]              public bool SuppressLogging    { get; set; } = false;

        [XmlElement("LogEvent")] public List<LogEvent> Log      { get; set; } = new List<LogEvent>();
        [XmlIgnore]              public LogEvent CurrentLog     { get; set; } = null;

        public LogEvent StartLogEvent(string topic, LogEvent.eEventType type, string user = null)
        {
            FinishLogEvent();
            CurrentLog = new LogEvent()
            {
                Topic = topic,
                Type = type,
                LocalUser = user ?? Program.config.LocalUser,
            };
            Log.Add(CurrentLog);
            return CurrentLog;
        }
        protected void LogPropertyChange(string propertyName, object oldValue, object newValue)
        {
            if (XmlLog.SuppressAllLogging || SuppressLogging) return;

            string ov = oldValue.ToString();
            string nv = newValue.ToString();
            if (!ov.Equals(nv))
            {
                if (CurrentLog == null)
                {
                    CurrentLog = StartLogEvent($"DataChange, starting with {propertyName}", LogEvent.eEventType.DataChange);
                }
                CurrentLog.SubEvents.Add(new LogSubEvent()
                {
                    Topic = $"{propertyName} changes from \"{oldValue.ToString()}\" to \"{newValue.ToString()}\"",
                    Type = LogEvent.eEventType.DataChange,
                });
            }
        }
        public void FinishLogEvent()
        {
            CurrentLog = null;
        }
    }
}
