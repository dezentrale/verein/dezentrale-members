﻿using System;
using System.Xml.Serialization;

using dezentrale.model;

namespace dezentrale.model.money
{
    [XmlInclude(typeof(BankTransfer))]
    [XmlInclude(typeof(CashTransfer))]
    public class MoneyTransfer : IEquatable<MoneyTransfer>, IComparable<MoneyTransfer>
    {
        public enum eTransferType
        {
            MembershipFee = LogEvent.eEventType.MembershipFee,
            MembershipPayment = LogEvent.eEventType.MembershipPayment,
            MembershipDonation = LogEvent.eEventType.MembershipDonation,
            Unassigned,
            RunningCost,
            Ignored,
        }

        //This is the connection to the rest of the database
        [XmlAttribute] public bool   AssignFixed        { get; set; } = false;
        [XmlElement] public string   Id                 { get; set; } = null;
        [XmlElement] public uint     MemberNumber       { get; set; } = 0;

        //Basic transfer information
        [XmlElement] public eTransferType TransferType  { get; set; } = eTransferType.Unassigned;
        [XmlElement] public DateTime ValutaDate         { get; set; }
        [XmlElement] public Int64    Amount             { get; set; }   //This is being held in fixed-point-2-decimals (e.g. -100,00EUR = -10000)
        [XmlElement] public string   Currency           { get; set; } = "EUR";
        [XmlElement] public string   TransferReason     { get; set; } = "";

        [XmlIgnore]  public string   AmountString       { get { return core.Utils.Int64FPToString(Amount);/* $"{((float) Amount / 100)}";*/ } }
        [XmlIgnore]  public string   ValutaDateString   { get { return ValutaDate.ToShortDateString(); } }

        public override string ToString() { return Id; }
        public MoneyTransfer()
        {
            Id = Guid.NewGuid().ToString();
        }

        public bool Equals(MoneyTransfer other) { return Equals(other, true); }
        public bool Equals(MoneyTransfer other, bool evaluateReason)
        {
            return       this.ValutaDate.Equals(other.ValutaDate)
                    &&  (this.Amount == other.Amount)
                    &&   this.GetType().Equals(other.GetType())
                    &&   this.Currency.Equals(other.Currency)
                    &&   this.TransferReason.Equals(other.TransferReason);
        }

        public int CompareTo(DateTime other)
        {
            return ValutaDate.CompareTo(other);
        }
        public int CompareTo(MoneyTransfer other)
        {
            if (other == null) return 1;
            else return CompareTo(other.ValutaDate);
        }
    }
}